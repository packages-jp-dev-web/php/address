<?php

namespace JPAddress\Controllers\Api\Address;

use JPAddress\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;
use JPAddress\Requests\AddressRequest;
use JPAddress\Models\Address\Address;
use JPAddress\Repositories\AddressRepository;
use JPAddress\Requests\AddressCascadeRequest;
use JPAddress\Resources\AddressResource;
use JPAddress\Resources\AddressCollection;
use JPAddress\Resources\NeighborhoodResource;

class AddressController extends Controller
{
    public function index(Request $request)
    {
        $query = Address::where('address', '!=', '');
        !$request->search ?: ($query = $query->search($request->search));
        if ($request->order && !$request->search) {
            $query = $query->orderBy($request->column, $request->order);
        }

        if ($request->paginate) {
            return new AddressCollection($query->paginate($request->paginate));
        }

        return new AddressCollection(
            new LengthAwarePaginator(
                $query->get(),
                $query->get()->count(),
                -1,
                null,
                ['path' => route('address.index')]
            )
        );
    }

    public function relationships(Address $address, $relationships)
    {
        switch ($relationships) {
            case 'neighborhood':
                return new NeighborhoodResource($address->neighborhood);
            default:
                return response()->noContent(200);
        }
    }

    public function store(AddressRequest $request)
    {
        if ($address = Address::create($request->all())) {
            return response(['id' => $address->id], 201);
        }

        return response()->noContent(400);
    }

    public function storeCascade(AddressCascadeRequest $request, AddressRepository $addressRepository)
    {
        $address = $addressRepository->createAddress($request->all());

        return new AddressResource($address);
    }

    public function show(Address $address)
    {
        return new AddressResource($address);
    }

    public function update(AddressRequest $request, Address $address)
    {
        if ($address->update($request->all())) {
            return response()->noContent(200);
        }

        return response()->noContent(400);
    }

    public function updateCascade(AddressCascadeRequest $request, AddressRepository $addressRepository, Address $address)
    {
        if ($addressRepository->updateAddres($address->id, $request->all())) {
            return response()->noContent(200);
        }

        return response()->noContent(400);
    }

    public function destroy(Address $address)
    {
        if ($address->delete()) {
            return response()->noContent(200);
        }

        return response()->noContent(400);
    }
}
