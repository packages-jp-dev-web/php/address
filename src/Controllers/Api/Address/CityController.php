<?php

namespace JPAddress\Controllers\Api\Address;

use JPAddress\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;
// Requests
use Illuminate\Http\Request;
use JPAddress\Requests\CityRequest;
// Models
use  JPAddress\Models\Address\City;
// Resource
use JPAddress\Resources\CityResource;
use JPAddress\Resources\CityCollection;
use JPAddress\Resources\NeighborhoodCollection;
use JPAddress\Resources\StateResource;

class CityController extends Controller
{
    public function index(Request $request)
    {
        $query = City::where('name', '!=', '');
        !$request->search ?: ($query = $query->search($request->search));
        if ($request->order && !$request->search) {
            $query = $query->orderBy($request->column, $request->order);
        }

        if ($request->paginate) {
            return new CityCollection($query->paginate($request->paginate));
        }

        return new CityCollection(
            new LengthAwarePaginator(
                $query->get(),
                $query->get()->count(),
                -1,
                null,
                ['path' => route('city.index')]
            )
        );
    }

    public function relationships(City $city, $relationships)
    {
        switch ($relationships) {
            case 'neighborhoods':
                return new NeighborhoodCollection($city->neighborhoods);
            case 'state':
                return new StateResource($city->state);
            default:
                return response()->noContent(200);
        }
    }

    public function store(CityRequest $request)
    {
        if ($city = City::create($request->all())) {
            return response(['id' => $city->id], 201);
        }

        return response()->noContent(400);
    }

    public function show(City $city)
    {
        return new CityResource($city);
    }

    public function update(CityRequest $request, City $city)
    {
        if ($city->update($request->all())) {
            return response()->noContent(200);
        }

        return response()->noContent(400);
    }

    public function destroy(City $city)
    {
        if ($city->delete()) {
            return response()->noContent(200);
        }

        return response()->noContent(400);
    }
}
