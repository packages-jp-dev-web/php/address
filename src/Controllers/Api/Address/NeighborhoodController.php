<?php

namespace JPAddress\Controllers\Api\Address;

use JPAddress\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;
use JPAddress\Requests\NeighborhoodRequest;
use JPAddress\Models\Address\Neighborhood;
use JPAddress\Resources\NeighborhoodResource;
use JPAddress\Resources\NeighborhoodCollection;
use JPAddress\Resources\CityResource;

class NeighborhoodController extends Controller
{
    public function index(Request $request)
    {
        $query = Neighborhood::where('name', '!=', '');
        !$request->search ?: ($query = $query->search($request->search));
        if ($request->order && !$request->search) {
            $query = $query->orderBy($request->column, $request->order);
        }

        if ($request->paginate) {
            return new NeighborhoodCollection($query->paginate($request->paginate));
        }

        return new NeighborhoodCollection(
            new LengthAwarePaginator(
                $query->get(),
                $query->get()->count(),
                -1,
                null,
                ['path' => route('neighborhood.index')]
            )
        );
    }

    public function relationships(Neighborhood $neighborhood, $relationships)
    {
        switch ($relationships) {
            case 'city':
                return new CityResource($neighborhood->city);
            default:
                return response()->noContent(200);
        }
    }

    public function store(NeighborhoodRequest $request)
    {
        if ($neighborhood = Neighborhood::create($request->all())) {
            return response(['id' => $neighborhood->id], 201);
        }

        return response()->noContent(400);
    }

    public function show(Neighborhood $neighborhood)
    {
        return new NeighborhoodResource($neighborhood);
    }

    public function update(NeighborhoodRequest $request, Neighborhood $neighborhood)
    {
        if ($neighborhood->update($request->all())) {
            return response()->noContent(200);
        }

        return response()->noContent(400);
    }

    public function destroy(Neighborhood $neighborhood)
    {
        if ($neighborhood->delete()) {
            return response()->noContent(200);
        }

        return response()->noContent(400);
    }
}
