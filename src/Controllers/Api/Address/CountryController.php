<?php

namespace JPAddress\Controllers\Api\Address;

use JPAddress\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;
use JPAddress\Requests\CountryRequest;
use JPAddress\Models\Address\Country;
use JPAddress\Resources\CountryCollection;
use JPAddress\Resources\CountryResource;
use JPAddress\Resources\StateCollection;

class CountryController extends Controller
{
    public function index(Request $request)
    {
        $query = $request->states ?
            Country::with('states') :
            Country::where('name', '!=', '');
        !$request->search ?: ($query = $query->search($request->search));
        if ($request->order && !$request->search) {
            $query = $query->orderBy($request->column, $request->order);
        }

        if ($request->paginate) {
            return new CountryCollection($query->paginate($request->paginate));
        }

        return new CountryCollection(
            new LengthAwarePaginator(
                $query->get(),
                $query->get()->count(),
                -1,
                null,
                ['path' => route('country.index')]
            )
        );
    }

    public function relationships(Country $country, $relationships)
    {
        if ($relationships == 'states') {
            return new StateCollection($country->states);
        }

        return response()->noContent(200);
    }

    public function store(CountryRequest $request)
    {
        if ($country = Country::create($request->all())) {
            return response(['id' => $country->id], 201);
        }

        return response()->noContent(400);
    }

    public function show(Country $country)
    {
        return new CountryResource($country);
    }

    public function update(CountryRequest $request, Country $country)
    {
        if ($country->update($request->all())) {
            return response()->noContent(200);
        }

        return response()->noContent(400);
    }

    public function destroy(Country $country)
    {
        if ($country->delete()) {
            return response()->noContent(200);
        }

        return response()->noContent(400);
    }
}
