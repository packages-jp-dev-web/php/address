<?php

namespace JPAddress\Controllers\Api\Address;

use JPAddress\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;
use JPAddress\Requests\StateRequest;
use JPAddress\Models\Address\State;
use JPAddress\Resources\StateResource;
use JPAddress\Resources\StateCollection;
use JPAddress\Resources\CountryResource;
use JPAddress\Resources\CityCollection;

class StateController extends Controller
{
    public function index(Request $request)
    {
        $query = State::where('name', '!=', '');
        !$request->search ?: ($query = $query->search($request->search));
        if ($request->order && !$request->search) {
            $query = $query->orderBy($request->column, $request->order);
        }

        if ($request->paginate) {
            return new StateCollection($query->paginate($request->paginate));
        }

        return new StateCollection(
            new LengthAwarePaginator(
                $query->get(),
                $query->get()->count(),
                -1,
                null,
                ['path' => route('state.index')]
            )
        );
    }

    public function relationships(State $state, $relationships)
    {
        switch ($relationships) {
            case 'cities':
                return new CityCollection($state->cities);
            case 'country':
                return new CountryResource($state->country);
            default:
                return response()->noContent(200);
        }
    }

    public function store(StateRequest $request)
    {
        if ($state = State::create($request->all())) {
            return response(['id' => $state->id], 201);
        }

        return response()->noContent(400);
    }

    public function show(State $state)
    {
        return new StateResource($state);
    }

    public function update(StateRequest $request, State $state)
    {
        if ($state->update($request->all())) {
            return response()->noContent(200);
        }

        return response()->noContent(400);
    }

    public function destroy(State $state)
    {
        if ($state->delete()) {
            return response()->noContent(200);
        }

        return response()->noContent(400);
    }
}
