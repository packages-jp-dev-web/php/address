<?php

/**
 * @var Illuminate\Database\Eloquent\Factory $factory
 */

use Faker\Generator as Faker;

use Faker\Provider\pt_BR\Address as Pt_BRAddress;
use JPAddress\Models\Address\Country;
use JPAddress\Models\Address\State;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(
    State::class,
    function (Faker $faker) {
        $faker->addProvider(new Pt_BRAddress($faker));
        $name = $faker->unique()->state();
        $count = State::whereName(Str::upper($name))->count();
        $initials = $faker->unique()->stateAbbr();
        $countInitials = State::whereInitials(Str::upper($initials))->count();

        return  [
            'name' => $count ? $name . '-' . ($count + 1) : $name,
            'initials' => $countInitials ? Str::limit($initials, 1, '') . ($count + 1) : $initials,
            'country_id' => function () {
                return factory(Country::class)->create()->id;
            },
        ];
    }
);
