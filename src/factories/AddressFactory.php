<?php

/**
 * @var Illuminate\Database\Eloquent\Factory $factory
 */

use Faker\Generator as Faker;
use Faker\Provider\pt_BR\Address as Pt_BRAddress;
use JPAddress\Models\Address\Address;
use JPAddress\Models\Address\Neighborhood;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(
    Address::class,
    function (Faker $faker) {
        $faker->addProvider(new Pt_BRAddress($faker));

        return  [
            'address' => $faker->streetName(),
            'number' => $faker->randomNumber(3),
            'complement' => $faker->text(10),
            'cep' => str_replace('-', '', $faker->postcode()),
            'longitude' => $faker->longitude(),
            'latitude' => $faker->latitude(),
            'neighborhood_id' => function () {
                return factory(Neighborhood::class)->create()->id;
            },
        ];
    }
);
