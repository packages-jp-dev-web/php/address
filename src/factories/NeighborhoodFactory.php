<?php

/**
 * @var Illuminate\Database\Eloquent\Factory $factory
 */

use Faker\Generator as Faker;

use Faker\Provider\pt_BR\Address as Pt_BRAddress;
use JPAddress\Models\Address\City;
use JPAddress\Models\Address\Neighborhood;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(
    Neighborhood::class,
    function (Faker $faker) {
        $faker->addProvider(new Pt_BRAddress($faker));
        $name = $faker->unique()->city();
        $count = Neighborhood::whereName(Str::upper($name))->count();

        return  [
            'name' => $count ? $name . '-' . ($count + 1) : $name,
            'city_id' => function () {
                return factory(City::class)->create()->id;
            },
        ];
    }
);
