<?php

namespace JPAddress\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;

class StateRequest extends FormRequest
{
    public function rules()
    {
        return [
            'name' => [
                'sometimes',
                'bail',
                'required',
                'max:100',
                Rule::unique('states')->where(
                    function ($query) {
                        return $query->where('country_id', $this->country_id);
                    }
                )->ignore($this->state),
            ],
            'initials' => [
                'sometimes', 'bail', 'required', 'max:2',
                Rule::unique('states')->where(
                    function ($query) {
                        return $query->where('country_id', $this->country_id);
                    }
                )->ignore($this->state),
            ],
            'country_id' => 'bail|required|uuid',
        ];
    }

    public function messages()
    {
        return [
            'max' => 'Limite o campo a no máximo :max caracteres.',
            'name.required' => 'Forneça um nome para o estado.',
            'initials.required' => 'A sigla do estado é obrigatória.',
            'name.unique' => 'Já existe um estado cadastrado com este nome.',
            'initials.unique' => 'Esta sigla de estado, já esta sendo usada.',
            'country_id.required' => 'Escolha um país para o estado.',
            'country_id.uuid' => 'Escolha um id de país válido para o estado.',
        ];
    }

    public function prepareForValidation()
    {
        $dataMerge = [];
        if ($this->name) {
            $dataMerge['name'] = Str::upper($this->name);
        }
        if ($this->initials) {
            $dataMerge['initials'] = Str::upper($this->initials);
        }
        $this->merge($dataMerge);
    }
}
