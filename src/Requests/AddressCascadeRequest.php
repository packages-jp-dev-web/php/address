<?php

namespace JPAddress\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;
use JPAddress\Requests\Traits\CommonRequestAddress;

class AddressCascadeRequest extends FormRequest
{
    use CommonRequestAddress;

    public function rules()
    {
        $method = 'rules' . Str::studly($this->method());

        return  $this->{$method}();
    }

    public function rulesPost(): array
    {
        return [
            'address' => ['nullable', 'max:100'],
            'number' => ['nullable', 'integer', 'min:0'],
            'complement' => ['nullable', 'max:15'],
            'cep' => ['nullable', 'formato_cep'],
            'latitude' => ['nullable', 'integer'],
            'longitude' => ['nullable', 'integer'],
            'neighborhood' => ['required', 'min:2', 'max:100'],
            'city' => ['required', 'min:2', 'max:100'],
            'state' => ['required', 'min:2', 'max:100'],
            'initials' => ['required', 'min:2', 'max:2'],
            'country' => ['nullable', 'min:2', 'max:100'],
        ];
    }

    public function rulesPatch(): array
    {
        return [
            'address' => ['sometimes', 'nullable', 'max:100'],
            'number' => ['sometimes', 'nullable', 'integer', 'min:0'],
            'complement' => ['sometimes', 'nullable', 'max:15'],
            'cep' => ['sometimes', 'nullable', 'formato_cep'],
            'latitude' => ['sometimes', 'nullable', 'integer'],
            'longitude' => ['sometimes', 'nullable', 'integer'],
            'neighborhood' => ['sometimes', 'required', 'min:2', 'max:100'],
            'city' => ['sometimes', 'required', 'min:2', 'max:100'],
            'state' => ['sometimes', 'required', 'min:2', 'max:100'],
            'initials' => ['sometimes', 'required', 'min:2', 'max:2'],
            'country' => ['sometimes', 'nullable', 'min:2', 'max:100'],
        ];
    }
}
