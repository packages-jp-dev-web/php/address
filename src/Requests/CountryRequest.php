<?php

namespace JPAddress\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;

class CountryRequest extends FormRequest
{
    public function rules()
    {
        return [
            'name' => [
                'sometimes',
                'bail',
                'required',
                'max:100',
                Rule::unique('countries')->ignore($this->country),
            ],
        ];
    }

    public function messages()
    {
        return [
            'max' => 'Limite o campo a no máximo :max caracteres.',
            'name.required' => 'Forneça um nome para o país.',
            'unique' => 'Já existe um país cadastrado com este nome.',
        ];
    }

    public function prepareForValidation()
    {
        $dataMerge = [];
        if ($this->name) {
            $dataMerge['name'] = Str::upper($this->name);
        }
        $this->merge($dataMerge);
    }
}
