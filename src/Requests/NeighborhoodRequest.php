<?php

namespace JPAddress\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;

class NeighborhoodRequest extends FormRequest
{
    public function rules()
    {
        return [
            'name' => [
                'sometimes',
                'bail',
                'required',
                'max:100',
                Rule::unique('neighborhoods')->where(
                    function ($query) {
                        return $query->where('city_id', $this->city_id);
                    }
                )->ignore($this->neighborhood),
            ],
            'city_id' => 'bail|required|uuid',
        ];
    }

    public function messages()
    {
        return [
            'max' => 'Limite o campo a no máximo :max caracteres.',
            'name.required' => 'Forneça um nome para o bairro.',
            'name.unique' => 'Já existe um bairro cadastrado com este nome.',
            'city_id.required' => 'Escolha uma cidade para o bairro.',
            'city_id.uuid' => 'Escolha um id de cidade válido para o bairro.',
        ];
    }

    public function prepareForValidation()
    {
        $dataMerge = [];
        if ($this->name) {
            $dataMerge['name'] = Str::upper($this->name);
        }
        $this->merge($dataMerge);
    }
}
