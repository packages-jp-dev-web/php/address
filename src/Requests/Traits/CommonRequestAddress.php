<?php

namespace JPAddress\Requests\Traits;

trait CommonRequestAddress
{
    public function messages()
    {
        return [
            'max' => 'Limite o campo a no máximo :max caracteres.',
            'integer' => 'Forneça um número válido.',
            'address.required' => 'Forneça o endereço.',
            'number.required' => 'Forneça um número para o endereço.',
            'number.min' => 'Forneça um número válido.',
            'cep.required' => 'Qual é o cep do endereço?',
            'cep.formato_cep' => 'Formato de cep aceito 99.999-99/99999-999.',
            'neighborhood_id.required' => 'Escolha um bairro para o endereço.',
            'neighborhood_id.exists' => 'Escolha um id de bairro válido.',
            'neighborhood.required' => 'Forneça um bairro para o endereço.',
            'city.required' => 'Forneça uma cidade para o endereço.',
            'state.required' => 'Forneça um estado para o endereço.',
            'initials.required' => 'Forneça a sigla do estado para o endereço.',
        ];
    }

    public function prepareForValidation()
    {
        $this->merge(
            [
                'number' => $this->not_number ? null : $this->number,
                'not_number' => $this->not_number ? true : false,
            ]
        );
    }
}
