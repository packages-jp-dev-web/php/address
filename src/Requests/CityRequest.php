<?php

namespace JPAddress\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;

class CityRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'sometimes',
                'bail',
                'required',
                'max:100',
                Rule::unique('cities')->where(
                    function ($query) {
                        return $query->where('state_id', $this->state_id);
                    }
                )->ignore($this->city),
            ],
            'state_id' => 'bail|required|uuid',
        ];
    }

    public function messages()
    {
        return [
            'max' => 'Limite o campo a no máximo :max caracteres.',
            'name.required' => 'Forneça um nome para a cidade.',
            'name.unique' => 'Já existe uma cidade cadastrada com este nome.',
            'state_id.required' => 'Escolha um estado para a cidade.',
            'state_id.uuid' => 'Escolha um id de estado válido para a cidade.',
        ];
    }

    public function prepareForValidation()
    {
        $dataMerge = [];
        if ($this->name) {
            $dataMerge['name'] = Str::upper($this->name);
        }
        $this->merge($dataMerge);
    }
}
