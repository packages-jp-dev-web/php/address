<?php

namespace JPAddress\Requests;

use Illuminate\Foundation\Http\FormRequest;
use JPAddress\Requests\Traits\CommonRequestAddress;

class AddressRequest extends FormRequest
{
    use CommonRequestAddress;

    public function rules()
    {
        return [
            'address' => 'sometimes|bail|nullable|max:100',
            'number' => 'sometimes|nullable|integer|min:1',
            'complement' => 'max:15',
            'cep' => 'sometimes|bail|nullable|formato_cep',
            'latitude' => 'sometimes|integer',
            'longitude' => 'sometimes|integer',
            'neighborhood_id' => ['bail', 'required', 'exists:neighborhoods,id'],
        ];
    }
}
