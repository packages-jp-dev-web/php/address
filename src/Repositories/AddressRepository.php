<?php

namespace JPAddress\Repositories;

use Illuminate\Support\Arr;
use JPAddress\Models\Address\Address;
use JPAddress\Models\Address\City;
use JPAddress\Models\Address\Country;
use JPAddress\Models\Address\Neighborhood;
use JPAddress\Models\Address\State;

class AddressRepository
{
    public const COLUMNS_ADDRESS = ['address', 'number', 'not_number', 'complement', 'cep', 'longitude', 'latitude'];
    public const COLUMNS_CREATE_UPDATE_NEIGHBORHOOD = ['neighborhood', 'city', 'state', 'initials', 'country'];

    public function createAddress(array $data): Address
    {
        $sanitiseData = Arr::only($data, self::COLUMNS_ADDRESS);
        $neighborhood = self::createNeighborhood(Arr::only($data, self::COLUMNS_CREATE_UPDATE_NEIGHBORHOOD));
        $sanitiseData['neighborhood_id'] = $neighborhood->id;

        return Address::create($sanitiseData);
    }

    public function updateAddres(string $addressId, array $data): Address
    {
        $address = Address::findOrFail($addressId);
        $sanitiseData = Arr::only($data, self::COLUMNS_ADDRESS);
        $neighborhood = $this->updateNeighborhoodCascadeOfAddress($address, Arr::only($data, self::COLUMNS_CREATE_UPDATE_NEIGHBORHOOD));
        $sanitiseData['neighborhood_id'] = $neighborhood->id;
        $address->update($sanitiseData);

        return $address;
    }

    private function createNeighborhood(array $data): Neighborhood
    {
        return Neighborhood::firstOrCreate(
            [
                'name' => $data['neighborhood'],
                'city_id' => City::firstOrCreate(
                    [
                        'name' => $data['city'],
                        'state_id' => State::firstOrCreate(
                            [
                                'name' => $data['state'],
                                'initials' => $data['initials'],
                                'country_id' => Country::firstOrCreate(['name' => Arr::get($data, 'country', 'Brasil')])->id,
                            ]
                        )->id,
                    ]
                )->id,
            ]
        );
    }

    private function updateNeighborhoodCascadeOfAddress(Address $address, array $data): Neighborhood
    {
        $country = Arr::has($data, ['country'])
            ? Country::firstOrCreate(['name' => $data['country']])
            : $address->neighborhood->city->state->country;
        $state = $this->updateState($address->neighborhood->city->state, $country, $data);
        $city = $this->updateCity($address->neighborhood->city, $state, $data);

        if (!Arr::has($data, ['neighborhood'])) {
            $address->neighborhood->update(['city_id' => $city->id]);

            return $address->neighborhood;
        }

        return Neighborhood::firstOrCreate(['name' => $data['neighborhood'], 'city_id' => $city->id]);
    }

    private function updateState(State $state, Country $country, array $data): State
    {
        if (!Arr::has($data, ['state', 'initials'])) {
            $state->update(['country_id' => $country->id]);

            return $state;
        }

        return State::firstOrCreate(['name' => $data['state'], 'initials' => $data['initials'], 'country_id' => $country->id]);
    }

    private function updateCity(City $city, State $state, array $data): City
    {
        if (!Arr::has($data, ['city'])) {
            $city->update(['state_id' => $state->id]);

            return $city;
        }

        return City::firstOrCreate(['name' => $data['city'], 'state_id' => $state->id]);
    }
}
