# Address

## Sobre o projeto

<img src="https://img.shields.io/static/v1?label=license&message=MIT&color=blue&style=%3CSTYLE%3E&logo=%3CLOGO%3E"> | <img src="https://img.shields.io/static/v1?label=composer&message=v1.0.0&color=success&style=%3CSTYLE%3E&logo=%3CLOGO%3E">

Este pacote foi criado na intenção de fornecer um gerenciamento para os endereços em seu site, que usa Laravel a partir da versão 6.x. Ele disponibiliza toda infraestrutura como: classes de models, controladores, migrations e uma api com as validações necessárias.

## Documentação

Em breve será disponibilizado a documentação da api
