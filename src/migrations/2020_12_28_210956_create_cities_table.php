<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'cities',
            function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->uuid('id')->primary();
                $table->string('state_id', 36);
                $table->foreign('state_id')
                    ->references('id')
                    ->on('states')
                    ->onDelete('cascade');
                $table->string('slug', 100);
                $table->string('name', 100);
                $table->timestamps();
            }
        );
        if (env('DB_CONNECTION') != 'sqlite') {
            DB::statement(
                'ALTER TABLE cities ADD FULLTEXT
            fulltexcities_index (name)'
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities');
    }
}
