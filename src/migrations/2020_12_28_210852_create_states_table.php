<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'states',
            function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->uuid('id')->primary();
                $table->string('country_id', 36);
                $table->foreign('country_id')
                    ->references('id')
                    ->on('countries')
                    ->onDelete('cascade');
                $table->string('slug', 100);
                $table->string('name', 100);
                $table->string('initials', 2);
                $table->timestamps();
            }
        );
        if (env('DB_CONNECTION') != 'sqlite') {
            DB::statement(
                'ALTER TABLE states ADD FULLTEXT
            fulltexstates_index (name, initials)'
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('states');
    }
}
