<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'countries',
            function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->uuid('id')->primary();
                $table->string('slug', 100);
                $table->string('name', 100)->unique();
                $table->timestamps();
            }
        );
        if (env('DB_CONNECTION') != 'sqlite') {
            DB::statement(
                'ALTER TABLE countries ADD FULLTEXT
            fulltexcountries_index (name)'
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
    }
}
