<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'addresses',
            function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->uuid('id')->primary();
                $table->string('neighborhood_id', 36);
                $table->foreign('neighborhood_id')
                    ->references('id')
                    ->on('neighborhoods')
                    ->onDelete('cascade');
                $table->string('address', 100)->nullable()->default(null);
                $table->integer('number')->nullable()->default(null);
                $table->boolean('not_number')->default(false);
                $table->string('complement', 15)->nullable()->default(null);
                $table->string('cep', 8)->nullable()->default(null);
                $table->integer('longitude')->nullable()->default(null);
                $table->integer('latitude')->nullable()->default(null);
                $table->timestamps();
            }
        );
        if (env('DB_CONNECTION') != 'sqlite') {
            DB::statement(
                'ALTER TABLE addresses ADD FULLTEXT
            fulltexaddresses_index (address, complement)'
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
