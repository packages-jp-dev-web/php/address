<?php

namespace JPAddress\Models\Address;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Str;
use JPAddress\Models\Concerns\SetSlug;
use JPAddress\Models\Concerns\UsesUuid;
use JPAddress\Models\Concerns\UtilityEloquent;

class State extends Model
{
    use SetSlug;
    use UsesUuid;
    use UtilityEloquent;
    protected $fillable = ['country_id', 'name', 'initials'];

    public function country(): BelongsTo
    {
        return $this->belongsTo(Country::class);
    }

    public function cities(): HasMany
    {
        return $this->hasMany(City::class);
    }

    public function scopeSearch($query, $search)
    {
        if (env('DB_CONNECTION') != 'production') {
            return $query->where('initials', 'like', "{$search}%")
                ->orWhere('name', 'like', "{$search}%");
        }
        $formatedString = $this->fullTextWildcards($search);
        $query
            ->selectRaw("*, MATCH (name, initials) AGAINST ('" . $formatedString . "' IN BOOLEAN MODE) AS relevance")
            ->whereRaw(
                'MATCH (name, initials) AGAINST (? IN BOOLEAN MODE)',
                $formatedString
            )->orWhere('initials', 'like', "{$search}%")
            ->orWhere('name', 'like', "{$search}%")
            ->orderBy('relevance');

        return $query;
    }

    protected static function boot()
    {
        parent::boot();
        static::saving(
            function ($model) {
                $model->name = Str::upper($model->name);
                $model->initials = Str::upper($model->initials);
            }
        );
    }
}
