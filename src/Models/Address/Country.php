<?php

namespace JPAddress\Models\Address;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Str;
use JPAddress\Models\Concerns\SetSlug;
use JPAddress\Models\Concerns\UsesUuid;
use JPAddress\Models\Concerns\UtilityEloquent;

class Country extends Model
{
    use SetSlug;
    use UsesUuid;
    use UtilityEloquent;
    protected $fillable = ['name'];

    public function states(): HasMany
    {
        return $this->hasMany(State::class);
    }

    public function scopeSearch($query, $search)
    {
        if (env('DB_CONNECTION') != 'production') {
            return $query->where('name', 'like', "{$search}%");
        }
        $formatedString = $this->fullTextWildcards($search);
        $query
            ->selectRaw("*, MATCH (name) AGAINST ('" . $formatedString . "' IN BOOLEAN MODE) AS relevance")
            ->whereRaw(
                'MATCH (name) AGAINST (? IN BOOLEAN MODE)',
                $formatedString
            )
            ->orWhere('name', 'like', "{$search}%")
            ->orderBy('relevance');

        return $query;
    }

    protected static function boot()
    {
        parent::boot();
        static::saving(
            function ($model) {
                $model->name = Str::upper($model->name);
            }
        );
    }
}
