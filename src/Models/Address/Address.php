<?php

namespace JPAddress\Models\Address;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use JPAddress\Models\Concerns\UsesUuid;
use JPAddress\Models\Concerns\UtilityEloquent;

class Address extends Model
{
    use UsesUuid;
    use UtilityEloquent;

    protected $fillable = ['neighborhood_id', 'address', 'number', 'not_number', 'complement', 'cep', 'longitude', 'latitude'];

    public function neighborhood(): BelongsTo
    {
        return $this->belongsTo(Neighborhood::class);
    }

    public function scopeSearch($query, $search)
    {
        if (env('DB_CONNECTION') != 'production') {
            return $query->where('address', 'like', "%{$search}%")
                ->orWhere('complement', 'like', "%{$search}%");
        }
        $formatedString = $this->fullTextWildcards($search);
        $query
            ->selectRaw("*, MATCH (address, complement) AGAINST ('" . $formatedString . "' IN BOOLEAN MODE) AS relevance")
            ->whereRaw(
                'MATCH (address, complement) AGAINST (? IN BOOLEAN MODE)',
                $formatedString
            )
            ->orWhere('address', 'like', "%{$search}%")
            ->orWhere('complement', 'like', "%{$search}%")
            ->orderBy('relevance');

        return $query;
    }

    protected static function boot()
    {
        parent::boot();
        static::saving(
            function ($model) {
                $model->cep = preg_replace('/[^0-9]/', '', $model->cep);
            }
        );
    }
}
