<?php

namespace JPAddress\Models\Concerns;

use Illuminate\Support\Str;

trait SetSlug
{
    protected static function bootSetSlug()
    {
        static::saving(
            function ($model) {
                $model->slug = Str::slug($model->name);
            }
        );
    }
}
