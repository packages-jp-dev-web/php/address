<?php

namespace JPAddress\Models\Concerns;

trait UtilityEloquent
{
    /**
     * Replaces spaces with full text search wildcards
     *
     * @param  string $term
     * @return string
     */
    public function fullTextWildcards($term)
    {
        // removing symbols used by MySQL
        $reservedSymbols = ['-', '+', '<', '>', '@', '(', ')', '~'];
        $term = str_replace($reservedSymbols, '', $term);

        $words = explode(' ', $term);

        foreach ($words as $key => $word) {
            /*
                   * applying + operator (required word) only big words
                   * because smaller ones are not indexed by mysql
                   */
            if (mb_strlen($word) >= 2) {
                $words[$key] = '+' . $word . '*';
            }
        }

        return implode(' ', $words);
    }
}
