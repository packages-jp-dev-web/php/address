<?php

namespace JPAddress\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AddressResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'type' => 'address',
            'id' => $this->id,
            'attributes' => [
                'address' => $this->address,
                'number' => $this->number,
                'not_number' => $this->not_number ? true : false,
                'complement' => $this->complement,
                'cep' => $this->cep,
                'latitude' => number_format($this->latitude, 2, '.', ''),
                'longitude' => number_format($this->longitude, 2, '.', ''),
                'created_at' => $this->created_at->format('Y-m-d H:i:s'),
            ],
            'relationships' => [
                'neighborhood' => ['data' => ['type' => 'neighborhood', 'id' => $this->neighborhood_id]],
            ],
        ];
    }

    public function with($request)
    {
        return ['included' => [new NeighborhoodResource($this->neighborhood)]];
    }
}
