<?php

namespace JPAddress\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CityCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return $this->collection->map(
            function ($city) {
                return new CityResource($city);
            }
        );
    }

    public function with($request)
    {
        return [
            'included' => $this->collection->pluck('state')->unique()->values()->map(
                function ($state) {
                    return new StateResource($state);
                }
            )->merge(
                $this->when(
                    !$this->collection
                        ->filter
                        ->relationLoaded('neighborhoods')
                        ->isEmpty() || $request->neighborhoods,
                    function () {
                        return $this->collection
                            ->pluck('neighborhoods')
                            ->unique()
                            ->values()
                            ->flatten()
                            ->map(
                                function ($neighborhood) {
                                    return new NeighborhoodResource($neighborhood);
                                }
                            );
                    }
                )
            ),
        ];
    }
}
