<?php

namespace JPAddress\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class NeighborhoodCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return $this->collection->map(
            function ($neighborhood) {
                return new NeighborhoodResource($neighborhood);
            }
        );
    }

    public function with($request)
    {
        return [
            'included' => $this->collection->pluck('city')->unique()->values()->map(
                function ($city) {
                    return new CityResource($city);
                }
            ),
        ];
    }
}
