<?php

namespace JPAddress\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class AddressCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return $this->collection->map(
            function ($address) {
                return new AddressResource($address);
            }
        );
    }

    public function with($request)
    {
        return ['included' => $this
            ->collection
            ->pluck('neighborhood')
            ->unique()
            ->values()
            ->map(
                function ($neighborhood) {
                    return new NeighborhoodResource($neighborhood);
                }
            ), ];
    }
}
