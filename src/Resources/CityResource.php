<?php

namespace JPAddress\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;

// Resources

class CityResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'type' => 'city',
            'id' => $this->id,
            'attributes' => [
                'name' => Str::title($this->name),
                'slug' => $this->slug,
                'created_at' => $this->created_at->format('Y-m-d H:i:s'),
            ],
            'relationships' => [
                'state' => [
                    'data' => [
                        'type' => 'state',
                        'id' => $this->state_id,
                    ],
                ],
                'neighborhoods' => $this->when(
                    $request->neighborhoods || $this->relationLoaded('neighborhoods'),
                    [
                        'data' => $this->neighborhoods->map(
                            function ($neighborhood) {
                                return [
                                    'type' => 'neighborhood',
                                    'id' => $neighborhood->id,
                                ];
                            }
                        ),
                    ]
                ),
            ],
        ];
    }

    public function with($request)
    {
        $included = collect([new StateResource($this->state)]);
        if ($request->neighborhoods || $this->relationLoaded('neighborhoods')) {
            $included = $included->merge(new NeighborhoodCollection($this->neighborhoods));
        }

        return [
            'included' => $included,
        ];
    }
}
