<?php

namespace JPAddress\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CountryCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return $this->collection->map(
            function ($country) {
                return new CountryResource($country);
            }
        );
    }

    public function with($request)
    {
        return [
            'included' => $this->when(
                !$this->collection
                    ->filter
                    ->relationLoaded('states')
                    ->isEmpty(),
                function () {
                    return $this->collection
                        ->pluck('states')
                        ->unique()
                        ->values()
                        ->flatten()
                        ->map(
                            function ($state) {
                                return new StateResource($state);
                            }
                        );
                }
            ),
        ];
    }
}
