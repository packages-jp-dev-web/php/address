<?php

namespace JPAddress\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class StateCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return $this->collection->map(
            function ($state) {
                return new StateResource($state);
            }
        );
    }

    public function with($request)
    {
        return [
            'included' => $this->collection
                ->pluck('country')
                ->unique()
                ->values()
                ->map(
                    function ($country) {
                        return new CountryResource($country);
                    }
                )->merge(
                    $this->when(
                        !$this->collection
                            ->filter
                            ->relationLoaded('cities')
                            ->isEmpty() || $request->cities,
                        function () {
                            return $this->collection
                                ->pluck('cities')
                                ->unique()
                                ->values()
                                ->flatten()
                                ->map(
                                    function ($city) {
                                        return new CityResource($city);
                                    }
                                );
                        }
                    )
                ),
        ];
    }
}
