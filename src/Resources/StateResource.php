<?php

namespace JPAddress\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;

class StateResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'type' => 'state',
            'id' => $this->id,
            'attributes' => [
                'slug' => $this->slug,
                'name' => Str::title($this->name),
                'initials' => $this->initials,
                'created_at' => $this->created_at->format('Y-m-d H:i:s'),
            ],
            'relationships' => [
                'country' => [
                    'data' => [
                        'type' => 'country',
                        'id' => $this->country_id,
                    ],
                ],
                'cities' => $this->when(
                    $request->cities || $this->relationLoaded('cities'),
                    [
                        'data' => $this->cities->map(
                            function ($city) {
                                return [
                                    'type' => 'city',
                                    'id' => $city->id,
                                ];
                            }
                        ),
                    ]
                ),
            ],
        ];
    }

    public function with($request)
    {
        $included = collect([new CountryResource($this->country)]);
        if ($request->cities || $this->relationLoaded('cities')) {
            $included = $included->merge(new CityCollection($this->cities));
        }

        return [
            'included' => $included,
        ];
    }
}
