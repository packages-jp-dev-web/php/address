<?php

namespace JPAddress\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;

class NeighborhoodResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'type' => 'neighborhood',
            'id' => $this->id,
            'attributes' => [
                'slug' => $this->slug,
                'name' => Str::title($this->name),
                'created_at' => $this->created_at->format('Y-m-d H:i:s'),
            ],
            'relationships' => [
                'city' => ['data' => ['type' => 'city', 'id' => $this->city_id]],
            ],
        ];
    }

    public function with($request)
    {
        return ['included' => [new CityResource($this->city)]];
    }
}
