<?php

namespace JPAddress\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;

class CountryResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'type' => 'country',
            'id' => $this->id,
            'attributes' => [
                'name' => Str::title($this->name),
                'slug' => $this->slug,
                'created_at' => $this->created_at->format('Y-m-d H:i:s'),
            ],
            'relationships' => [
                'states' => $this->when(
                    $request->states || $this->relationLoaded('states'),
                    [
                        'data' => $this->states->map(
                            function ($state) {
                                return [
                                    'type' => 'state',
                                    'id' => $state->id,
                                ];
                            }
                        ),
                    ]
                ),
            ],
        ];
    }

    public function with($request)
    {
        return [
            'included' => $this->when(
                $request->states || $this->relationLoaded('states'),
                function () {
                    return new StateCollection($this->states);
                }
            ),
        ];
    }
}
