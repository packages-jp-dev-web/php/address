<?php

use Illuminate\Support\Facades\Route;
use JPAddress\Controllers\Api\Address\CountryController;
use JPAddress\Controllers\Api\Address\StateController;
use JPAddress\Controllers\Api\Address\CityController;
use JPAddress\Controllers\Api\Address\NeighborhoodController;
use JPAddress\Controllers\Api\Address\AddressController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('api/')
    ->middleware('api')
    ->group(
        function () {
            Route::get('country/{country}/relationships/{relationships}', [CountryController::class, 'relationships']);
            Route::get('state/{state}/relationships/{relationships}', [StateController::class, 'relationships']);
            Route::get('city/{city}/relationships/{relationships}', [CityController::class, 'relationships']);
            Route::get('neighborhood/{neighborhood}/relationships/{relationships}', [NeighborhoodController::class, 'relationships']);
            Route::get('address/{address}/relationships/{relationships}', [AddressController::class, 'relationships']);

            Route::apiResources(
                [
                    'country' => CountryController::class,
                    'state' => StateController::class,
                    'city' => CityController::class,
                    'neighborhood' => NeighborhoodController::class,
                    'address' => AddressController::class,
                ]
            );

            Route::post('address-cascade', [AddressController::class, 'storeCascade'])->name('address.store_cascade');
            Route::patch('address-cascade/{address}', [AddressController::class, 'updateCascade'])->name('address.update_cascade');
        }
    );
