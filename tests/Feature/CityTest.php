<?php

namespace JPAddress\Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use JPAddress\Tests\TestCase;
use Illuminate\Support\Str;
// Models
use JPAddress\Models\Address\State;
use JPAddress\Models\Address\City;
use JPAddress\Models\Address\Neighborhood;

// When testing inside of a Laravel installation, the base class would be Tests\TestCase
class CityTest extends TestCase
{
    use DatabaseMigrations;
    protected $baseUrl = '/api/city';

    // Use annotation @test so that PHPUnit knows about the test

    /** @test
     * @group city
     * @group validate
     * @group city-validate
     */
    public function validate()
    {
        $state = factory(State::class)->create();
        $dataRequest = ['name' => 'Qualquer', 'state_id' => $state->id];
        $validation = [
            ['column' => 'name', 'value' => ''],
            ['column' => 'name', 'value' => Str::random(101)],
            ['column' => 'name', 'value' => 'Caldas Novas'],
            ['column' => 'state_id', 'value' => ''],
            ['column' => 'state_id', 'value' => 'lklkjj'],
        ];
        $response = $this->postJson($this->baseUrl, [
            'state_id' => $state->id,
            'name' => 'Caldas Novas',
        ]);
        $response->assertStatus(201);

        foreach ($validation as $key => $value) {
            $aux = $dataRequest;
            $aux[$value['column']] = $value['value'];
            $response = $this->postJson($this->baseUrl, $aux);
            $response->assertStatus(422);
        }
        $city = City::first();
        $response = $this->patchJson("{$this->baseUrl}/{$city->id}", ['state_id' => $state->id, 'name' => 'Caldas Novas']);
        $response->assertStatus(200);
    }

    /** @test
     * @group city
     * @group index
     * @group city-index
     */
    public function index()
    {
        $state = factory(State::class)->create(['name' => 'Minas Gerais']);
        $stateSp = factory(State::class)->create(['name' => 'São Paulo']);
        factory(City::class, 5)->create(['state_id' => $state->id]);
        $cityFirst = factory(City::class)->create(['state_id' => $stateSp->id]);
        factory(City::class)->create(['state_id' => $stateSp->id]);

        $cities = City::all();
        $response = $this->getJson($this->baseUrl);
        $response->assertStatus(200);
        $dataResult = ['data' => [], 'meta' => [
            'current_page' => 1,
            'from' => 1,
            'last_page' => 1,
            'links' => [
                [
                    'url' => null,
                    'label' => '&laquo; Previous',
                    'active' => false,
                ],
                [
                    'url' => 'http://localhost/api/city?page=1',
                    'label' => '1',
                    'active' => true,
                ],
                [
                    'url' => null,
                    'label' => 'Next &raquo;',
                    'active' => false,
                ],
            ],
            'path' => 'http://localhost/api/city',
            'per_page' => -1,
            'to' => 7,
            'total' => 7,
        ], 'links' => [
            'first' => 'http://localhost/api/city?page=1',
            'last' => 'http://localhost/api/city?page=1',
            'prev' => null,
            'next' => null,
        ], 'included' => [
            ['type' => 'state', 'id' => $state->id, 'attributes' => ['slug' => $state->slug, 'initials' => $state->initials, 'name' => Str::title($state->name), 'created_at' => $state->created_at->format('Y-m-d H:i:s')], 'relationships' => ['country' => ['data' => ['type' => 'country', 'id' => $state->country_id]]]],
            ['type' => 'state', 'id' => $stateSp->id, 'attributes' => ['slug' => $stateSp->slug, 'initials' => $stateSp->initials, 'name' => Str::title($stateSp->name), 'created_at' => $stateSp->created_at->format('Y-m-d H:i:s')], 'relationships' => ['country' => ['data' => ['type' => 'country', 'id' => $stateSp->country_id]]]],
        ]];
        $dataResult['data'] = array_values($cities->map(function ($city) {
            return [
                'type' => 'city',
                'id' => $city->id,
                'attributes' => [
                    'name' => Str::title($city->name),
                    'slug' => $city->slug, 'created_at' => $city->created_at->format('Y-m-d H:i:s'),
                ],
                'relationships' => [
                    'state' => ['data' => ['id' => $city->state_id, 'type' => 'state']],
                ],
            ];
        })->toArray());
        $dataApi = json_decode($response->getContent(), true);
        $this->assertEquals(2, count($dataApi['included']));
        $this->assertEqualsCanonicalizing($dataResult, $dataApi);
        $this->assertEquals(7, count($dataApi['data']));

        // Test search
        // Search per name with relation cities
        $neighborhoods = factory(Neighborhood::class, 2)->create(['city_id' => $cityFirst->id]);
        $response = $this->json('GET', $this->baseUrl, ['neighborhoods' => true, 'search' => $cityFirst->name]);
        $dataApi = json_decode($response->getContent(), true);
        $this->assertEquals(1, count($dataApi['data']));
        $this->assertEqualsCanonicalizing([
            [
                'type' => 'city', 'id' => $cityFirst->id, 'attributes' => [
                    'name' => Str::title($cityFirst->name),
                    'slug' => $cityFirst->slug, 'created_at' => $cityFirst->created_at->format('Y-m-d H:i:s'),
                ], 'relationships' => [
                    'state' => ['data' => ['id' => $cityFirst->state_id, 'type' => 'state']],
                    'neighborhoods' => ['data' => $neighborhoods->map(function ($neighborhood) {
                        return ['type' => 'neighborhood', 'id' => $neighborhood->id];
                    })->toArray()],
                ],
            ],
        ], $dataApi['data']);
        $included = array_values($neighborhoods->map(function ($neighborhood) {
            return [
                'type' => 'neighborhood',
                'id' => $neighborhood->id,
                'attributes' => ['name' => Str::title($neighborhood->name), 'slug' => $neighborhood->slug, 'created_at' => $neighborhood->created_at->format('Y-m-d H:i:s')],
                'relationships' => ['city' => ['data' => ['type' => 'city', 'id' => $neighborhood->city_id]]],
            ];
        })->toArray());
        array_push($included, ['type' => 'state', 'id' => $cityFirst->state_id, 'attributes' => ['name' => Str::title($cityFirst->state->name), 'slug' => $cityFirst->state->slug, 'initials' => $cityFirst->state->initials, 'created_at' => $cityFirst->state->created_at->format('Y-m-d H:i:s')], 'relationships' => ['country' => ['data' => ['type' => 'country', 'id' => $cityFirst->state->country_id]]]]);
        $this->assertEqualsCanonicalizing($included, $dataApi['included']);

        // Test paginate
        $response = $this->json('GET', $this->baseUrl, ['paginate' => 2]);
        $dataApi = json_decode($response->getContent(), true);
        $this->assertEquals(2, count($dataApi['data']));

        // Test order
        $dataNews = ['a', 'b', 'c', 'd', 'e'];
        City::where('name', '!=', '')->delete();
        $this->assertEquals(0, City::count());
        foreach ($dataNews as $value) {
            City::create(['state_id' => $state->id, 'name' => $value]);
        }
        $response = $this->json('GET', $this->baseUrl, ['order' => 'asc', 'column' => 'name']);
        $dataApi = json_decode($response->getContent(), true);
        foreach ($dataApi['data'] as $key => $state) {
            $this->assertEquals($dataNews[$key], $state['attributes']['slug']);
        }
        $response = $this->json('GET', $this->baseUrl, ['order' => 'desc', 'column' => 'name']);
        $dataApi = json_decode($response->getContent(), true);
        $i = 4;
        foreach ($dataApi['data'] as $key => $state) {
            $this->assertEquals($dataNews[$i], $state['attributes']['slug']);
            $i--;
        }
    }

    /** @test
     * @group city
     * @group relationships
     * @group city-relationships
     */
    public function relationships()
    {
        $city = factory(City::class)->create();
        $neighborhoods = factory(Neighborhood::class, 3)->create(['city_id' => $city->id]);
        $response = $this->getJson("{$this->baseUrl}/{$city->id}/relationships/neighborhoods");
        $response->assertStatus(200);
        $dataApi = json_decode($response->getContent(), true);
        $this->assertEquals(3, count($dataApi['data']));
        $this->assertEqualsCanonicalizing($neighborhoods->map(function ($neighborhood) {
            return [
                'type' => 'neighborhood', 'id' => $neighborhood->id, 'attributes' => ['slug' => $neighborhood->slug, 'name' => Str::title($neighborhood->name), 'created_at' => $neighborhood->created_at->format('Y-m-d H:i:s')],
                'relationships' => ['city' => ['data' => ['type' => 'city', 'id' => $neighborhood->city_id]]],
            ];
        })->toArray(), $dataApi['data']);
        $response = $this->getJson("{$this->baseUrl}/{$city->id}/relationships/state");
        $response->assertStatus(200);
        $dataApi = json_decode($response->getContent(), true);
        $this->assertEqualsCanonicalizing([
            'type' => 'state',
            'id' => $city->state->id,
            'attributes' => [
                'slug' => $city->state->slug,
                'initials' => $city->state->initials,
                'name' => Str::title($city->state->name), 'created_at' => $city->state->created_at->format('Y-m-d H:i:s'),
            ],
            'relationships' => ['country' => ['data' => ['type' => 'country', 'id' => $city->state->country_id]]],
        ], $dataApi['data']);
    }

    /** @test
     * @group city
     * @group show
     * @group city-show
     */
    public function show()
    {
        $city = factory(City::class)->create();
        $neighborhoods = factory(Neighborhood::class, 3)->create(['city_id' => $city->id]);
        $response = $this->json('GET', "{$this->baseUrl}/{$city->id}", ['neighborhoods' => true]);
        $response->assertStatus(200);
        $dataApi = json_decode($response->getContent(), true);
        $included = $neighborhoods->map(function ($neighborhood) {
            return ['type' => 'neighborhood', 'id' => $neighborhood->id, 'attributes' => ['name' => Str::title($neighborhood->name), 'slug' => $neighborhood->slug, 'created_at' => $neighborhood->created_at->format('Y-m-d H:i:s')], 'relationships' => [
                'city' => ['data' => ['type' => 'city', 'id' => $neighborhood->city_id]],
            ]];
        })->toArray();
        array_push(
            $included,
            [
                'type' => 'state', 'id' => $city->state->id,
                'attributes' => ['name' => Str::title($city->state->name), 'slug' => $city->state->slug, 'initials' => $city->state->initials, 'created_at' => $city->state->created_at->format('Y-m-d H:i:s')],
                'relationships' => ['country' => ['data' => ['type' => 'country', 'id' => $city->state->country->id]]],
            ]
        );
        $this->assertEqualsCanonicalizing($included, $dataApi['included']);
        $this->assertEqualsCanonicalizing([
            'data' => [
                'type' => 'city',
                'id' => $city->id,
                'attributes' => [
                    'name' => Str::title($city->name),
                    'slug' => $city->slug, 'created_at' => $city->created_at->format('Y-m-d H:i:s'),
                ],
                'relationships' => [
                    'state' => [
                        'data' => [
                            'type' => 'state',
                            'id' => $city->state_id,
                        ],
                    ],
                    'neighborhoods' => [
                        'data' => $neighborhoods->map(function ($neighborhood) {
                            return [
                                'type' => 'neighborhood',
                                'id' => $neighborhood->id,
                            ];
                        })->toArray(),
                    ],
                ],
            ],
            'included' => $included,
        ], $dataApi);
    }

    /** @test
     * @group city
     * @group store
     * @group city-store
     */
    public function store()
    {
        $this->assertNull(State::first());
        $state = factory(State::class)->create();
        $response = $this->postJson($this->baseUrl, [
            'state_id' => $state->id,
            'name' => 'São Paulo',
        ]);
        $response->assertStatus(201);
        $city = City::first();
        $this->assertNotNull($city);
        $this->assertEquals(Str::title($city->name), 'São Paulo');
        $this->assertEquals($city->state_id, $state->id);
    }

    /** @test
     * @group city
     * @group update
     * @group city-update
     */
    public function update()
    {
        $state = factory(State::class)->create();
        $stateUpdate = factory(State::class)->create();
        $city = factory(City::class)->create(['state_id' => $state->id]);
        $this->assertNotNull(State::first());
        $response = $this->patchJson("{$this->baseUrl}/{$city->id}", ['name' => 'Caldas', 'state_id' => $stateUpdate->id]);
        $response->assertStatus(200);
        $city = City::first();
        $this->assertNotNull($city);
        $this->assertEquals(Str::title($city->name), 'Caldas');
        $this->assertEquals($city->state_id, $stateUpdate->id);
    }

    /** @test
     * @group city
     * @group destroy
     * @group city-destroy
     */
    public function destroy()
    {
        $city = factory(City::class)->create();
        factory(Neighborhood::class, 5)->create(['city_id' => $city->id]);
        $this->assertNotNull(City::first());
        $this->assertEquals(5, Neighborhood::count());
        $response = $this->deleteJson("{$this->baseUrl}/{$city->id}");
        $response->assertStatus(200);
        $city = City::first();
        $this->assertNull($city);
        $this->assertEquals(0, Neighborhood::count());
    }
}
