<?php

namespace JPAddress\Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use JPAddress\Tests\TestCase;
use Illuminate\Support\Str;
// Models
use JPAddress\Models\Address\State;
use JPAddress\Models\Address\Country;
use JPAddress\Models\Address\City;

// When testing inside of a Laravel installation, the base class would be Tests\TestCase
class StateTest extends TestCase
{
    use RefreshDatabase;
    protected $baseUrl = '/api/state';

    /** @test
     * @group state
     * @group validate
     * @group state-validate
     */
    public function validate()
    {
        $country = factory(Country::class)->create();
        $dataRequest = ['name' => 'Qualquer', 'initials' => 'TT', 'country_id' => $country->id];
        $validation = [
            ['column' => 'name', 'value' => ''],
            ['column' => 'name', 'value' => Str::random(101)],
            ['column' => 'name', 'value' => 'Minas Gerais'],
            ['column' => 'initials', 'value' => ''],
            ['column' => 'initials', 'value' => Str::random(3)],
            ['column' => 'initials', 'value' => 'mg'],
            ['column' => 'country_id', 'value' => ''],
            ['column' => 'country_id', 'value' => 'lklkjj'],
        ];
        $response = $this->postJson($this->baseUrl, [
            'country_id' => $country->id,
            'name' => 'Minas Gerais',
            'initials' => 'mg',
        ]);
        $response->assertStatus(201);

        foreach ($validation as $key => $value) {
            $aux = $dataRequest;
            $aux[$value['column']] = $value['value'];
            $response = $this->postJson($this->baseUrl, $aux);
            $response->assertStatus(422);
        }
        $state = State::first();
        $response = $this->patchJson("{$this->baseUrl}/{$state->id}", ['country_id' => $country->id, 'name' => 'Minas Gerais']);
        $response->assertStatus(200);
    }

    /** @test
     * @group state
     * @group index
     * @group state-index
     */
    public function index()
    {
        $country = factory(Country::class)->create();
        $countryBrasil = factory(Country::class)->create(['name' => 'Brasil']);
        factory(State::class, 1)->create(['country_id' => $country->id, 'name' => 'Goiás']);
        factory(State::class)->create(['country_id' => $countryBrasil->id, 'name' => 'Minas Gerais', 'initials' => 'MG']);
        factory(State::class)->create(['country_id' => $countryBrasil->id, 'name' => 'São Paulo', 'initials' => 'SP']);

        $states = State::all();
        $response = $this->getJson($this->baseUrl);
        $response->assertStatus(200);
        $dataResult = ['data' => [], 'meta' => [
            'current_page' => 1,
            'from' => 1,
            'last_page' => 1,
            'links' => [
                [
                    'url' => null,
                    'label' => '&laquo; Previous',
                    'active' => false,
                ],
                [
                    'url' => 'http://localhost/api/state?page=1',
                    'label' => '1',
                    'active' => true,
                ],
                [
                    'url' => null,
                    'label' => 'Next &raquo;',
                    'active' => false,
                ],
            ],
            'path' => 'http://localhost/api/state',
            'per_page' => -1,
            'to' => 3,
            'total' => 3,
        ], 'links' => [
            'first' => 'http://localhost/api/state?page=1',
            'last' => 'http://localhost/api/state?page=1',
            'prev' => null,
            'next' => null,
        ], 'included' => [
            ['type' => 'country', 'id' => $country->id, 'attributes' => [
                'slug' => $country->slug, 'name' => Str::title($country->name),
                'created_at' => $country->created_at->format('Y-m-d H:i:s'),
            ], 'relationships' => []],
            ['type' => 'country', 'id' => $countryBrasil->id, 'attributes' => [
                'slug' => $countryBrasil->slug, 'name' => Str::title($countryBrasil->name),
                'created_at' => $country->created_at->format('Y-m-d H:i:s'),
            ], 'relationships' => []],
        ]];
        $dataResult['data'] = array_values($states->map(function ($state) {
            return [
                'type' => 'state',
                'id' => $state->id,
                'attributes' => [
                    'name' => Str::title($state->name),
                    'slug' => $state->slug,
                    'initials' => $state->initials,
                    'created_at' => $state->created_at->format('Y-m-d H:i:s'),
                ],
                'relationships' => [
                    'country' => ['data' => ['id' => $state->country_id, 'type' => 'country']],
                ],
            ];
        })->toArray());
        $dataApi = json_decode($response->getContent(), true);
        $this->assertEquals(2, count($dataApi['included']));
        $this->assertEqualsCanonicalizing($dataResult, $dataApi);
        $this->assertEquals(3, count($dataApi['data']));

        // Test search
        // Search per name with relation cities
        $stateChain = State::where('initials', 'MG')->first();
        $cities = factory(City::class, 2)->create(['state_id' => $stateChain->id]);
        $response = $this->json('GET', $this->baseUrl, ['cities' => true, 'search' => 'Minas Gerais']);
        $dataApi = json_decode($response->getContent(), true);
        $this->assertEquals(1, count($dataApi['data']));
        $this->assertEqualsCanonicalizing([
            [
                'type' => 'state', 'id' => $stateChain->id, 'attributes' => [
                    'name' => Str::title($stateChain->name),
                    'slug' => $stateChain->slug,
                    'initials' => $stateChain->initials,
                    'created_at' => $stateChain->created_at->format('Y-m-d H:i:s'),
                ], 'relationships' => [
                    'country' => ['data' => ['id' => $stateChain->country_id, 'type' => 'country']],
                    'cities' => ['data' => $cities->map(function ($city) {
                        return ['type' => 'city', 'id' => $city->id];
                    })->toArray()],
                ],
            ],
        ], $dataApi['data']);
        $included = array_values($cities->map(function ($city) {
            return [
                'type' => 'city',
                'id' => $city->id,
                'attributes' => [
                    'name' => Str::title($city->name), 'slug' => $city->slug,
                    'created_at' => $city->created_at->format('Y-m-d H:i:s'),
                ],
                'relationships' => ['state' => ['data' => ['type' => 'state', 'id' => $city->state_id]]],
            ];
        })->toArray());
        array_push($included, ['type' => 'country', 'id' => $stateChain->country_id, 'attributes' => [
            'name' => Str::title($stateChain->country->name), 'slug' => $stateChain->country->slug,
            'created_at' => $stateChain->created_at->format('Y-m-d H:i:s'),
        ], 'relationships' => []]);
        $this->assertEqualsCanonicalizing($included, $dataApi['included']);

        $response = $this->json('GET', $this->baseUrl, ['search' => 'MG']);
        $dataApi = json_decode($response->getContent(), true);
        $this->assertEquals(1, count($dataApi['data']));
        $this->assertEquals('minas-gerais', $dataApi['data'][0]['attributes']['slug']);

        // Test paginate
        $response = $this->json('GET', $this->baseUrl, ['paginate' => 2]);
        $dataApi = json_decode($response->getContent(), true);
        $this->assertEquals(2, count($dataApi['data']));

        // Test order
        $dataNews = ['a', 'b', 'c', 'd', 'e'];
        State::where('name', '!=', '')->delete();
        $this->assertEquals(0, State::count());
        foreach ($dataNews as $value) {
            State::create(['country_id' => $countryBrasil->id, 'name' => $value, 'initials' => $value]);
        }
        $response = $this->json('GET', $this->baseUrl, ['order' => 'asc', 'column' => 'name']);
        $dataApi = json_decode($response->getContent(), true);
        foreach ($dataApi['data'] as $key => $state) {
            $this->assertEquals($dataNews[$key], $state['attributes']['slug']);
        }
        $response = $this->json('GET', $this->baseUrl, ['order' => 'desc', 'column' => 'name']);
        $dataApi = json_decode($response->getContent(), true);
        $i = 4;
        foreach ($dataApi['data'] as $key => $state) {
            $this->assertEquals($dataNews[$i], $state['attributes']['slug']);
            $i--;
        }
    }

    /** @test
     * @group state
     * @group relationships
     * @group state-relationships
     */
    public function relationships()
    {
        $country = factory(Country::class)->create();
        $state = factory(State::class)->create(['country_id' => $country->id]);
        $cities = factory(City::class, 3)->create(['state_id' => $state->id]);
        $response = $this->getJson("{$this->baseUrl}/{$state->id}/relationships/cities");
        $response->assertStatus(200);
        $dataApi = json_decode($response->getContent(), true);
        $this->assertEquals(3, count($dataApi['data']));
        $this->assertEqualsCanonicalizing($cities->map(function ($city) {
            return [
                'type' => 'city', 'id' => $city->id, 'attributes' => [
                    'slug' => $city->slug, 'name' => Str::title($city->name),
                    'created_at' => $city->created_at->format('Y-m-d H:i:s'),
                ],
                'relationships' => ['state' => ['data' => ['type' => 'state', 'id' => $city->state_id]]],
            ];
        })->toArray(), $dataApi['data']);
        $response = $this->getJson("{$this->baseUrl}/{$state->id}/relationships/country");
        $response->assertStatus(200);
        $dataApi = json_decode($response->getContent(), true);
        $this->assertEqualsCanonicalizing([
            'type' => 'country',
            'id' => $country->id,
            'attributes' => [
                'slug' => $country->slug,
                'name' => Str::title($country->name),
                'created_at' => $country->created_at->format('Y-m-d H:i:s'),
            ],
            'relationships' => [],
        ], $dataApi['data']);
    }

    /** @test
     * @group state
     * @group show
     * @group state-show
     */
    public function show()
    {
        $country = factory(Country::class)->create();
        $state = factory(State::class)->create(['country_id' => $country->id]);
        $cities = factory(City::class, 3)->create(['state_id' => $state->id]);
        $response = $this->json('GET', "{$this->baseUrl}/{$state->id}", ['cities' => true]);
        $response->assertStatus(200);
        $dataApi = json_decode($response->getContent(), true);
        $included = $cities->map(function ($city) {
            return ['type' => 'city', 'id' => $city->id, 'attributes' => [
                'name' => Str::title($city->name), 'slug' => $city->slug,
                'created_at' => $city->created_at->format('Y-m-d H:i:s'),
            ], 'relationships' => [
                'state' => ['data' => ['type' => 'state', 'id' => $city->state_id]],
            ]];
        })->toArray();
        array_push(
            $included,
            ['type' => 'country', 'id' => $country->id, 'attributes' => [
                'name' => Str::title($country->name), 'slug' => $country->slug,
                'created_at' => $country->created_at->format('Y-m-d H:i:s'),
            ], 'relationships' => []]
        );
        $this->assertEqualsCanonicalizing($included, $dataApi['included']);
        $this->assertEqualsCanonicalizing([
            'data' => [
                'type' => 'state',
                'id' => $state->id,
                'attributes' => [
                    'name' => Str::title($state->name),
                    'initials' => $state->initials,
                    'slug' => $state->slug,
                    'created_at' => $state->created_at->format('Y-m-d H:i:s'),
                ],
                'relationships' => [
                    'country' => [
                        'data' => [
                            'type' => 'country',
                            'id' => $state->country_id,
                        ],
                    ],
                    'cities' => [
                        'data' => $cities->map(function ($city) {
                            return [
                                'type' => 'city',
                                'id' => $city->id,
                            ];
                        })->toArray(),
                    ],
                ],
            ],
            'included' => $included,
        ], $dataApi);
    }

    /** @test
     * @group state
     * @group store
     * @group state-store
     */
    public function store()
    {
        $this->assertNull(State::first());
        $country = factory(Country::class)->create();
        $response = $this->postJson($this->baseUrl, [
            'country_id' => $country->id,
            'name' => 'São Paulo',
            'initials' => 'SP',
        ]);
        $response->assertStatus(201);
        $state = State::first();
        $this->assertNotNull($state);
        $this->assertEquals(Str::title($state->name), 'São Paulo');
        $this->assertEquals($state->initials, 'SP');
        $this->assertEquals($state->country_id, $country->id);
    }

    /** @test
     * @group state
     * @group update
     * @group state-update
     */
    public function update()
    {
        $country = factory(Country::class)->create();
        $countryBrasil = factory(Country::class)->create(['name' => 'Brasil']);
        $state = factory(State::class)->create(['country_id' => $country->id]);
        $this->assertNotNull(State::first());

        $response = $this->patchJson("{$this->baseUrl}/{$state->id}", ['name' => 'Minas Gerais', 'initials' => 'MG', 'country_id' => $countryBrasil->id]);
        $response->assertStatus(200);
        $state = State::first();
        $this->assertNotNull($state);
        $this->assertEquals(Str::title($state->name), 'Minas Gerais');
        $this->assertEquals($state->initials, 'MG');
        $this->assertEquals($state->country_id, $countryBrasil->id);
    }

    /** @test
     * @group state
     * @group destroy
     * @group state-destroy
     */
    public function destroy()
    {
        $country = factory(Country::class)->create();
        $state = factory(State::class)->create(['country_id' => $country->id]);
        factory(City::class, 5)->create(['state_id' => $state->id]);
        $this->assertNotNull(State::first());
        $this->assertEquals(5, City::count());
        $response = $this->deleteJson("{$this->baseUrl}/{$state->id}");
        $response->assertStatus(200);
        $state = State::first();
        $this->assertNull($state);
        $this->assertEquals(0, City::count());
    }
}
