<?php

namespace JPAddress\Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use JPAddress\Tests\TestCase;
use Illuminate\Support\Str;
use JPAddress\Models\Address\State;
use JPAddress\Models\Address\Country;
use JPAddress\Models\Address\City;
use JPAddress\Models\Address\Neighborhood;
use JPAddress\Models\Address\Address;

class AddressTest extends TestCase
{
    use RefreshDatabase;
    protected $baseUrl = '/api/address';

    /**
     * @group address
     * @group validate
     * @group address-validate
     */
    public function testValidate()
    {
        $country = factory(Country::class)->create();
        $state = factory(State::class)->create(['country_id' => $country->id]);
        $city = factory(City::class)->create(['state_id' => $state->id]);
        $neighborhood = factory(Neighborhood::class)->create(['city_id' => $city->id]);
        $dataRequest = [
            'neighborhood_id' => $neighborhood->id,
            'address' => 'Rua Amazonas',
            'number' => 23,
            'complement' => 'casa 2',
            'cep' => '37701-234',
            'latitude' => -39,
            'longitude' => 69,
        ];
        $validation = [
            ['column' => 'address', 'value' => Str::random(101), 'message' => 'Limite o campo a no máximo 100 caracteres.'],
            ['column' => 'number', 'value' => 0, 'message' => 'Forneça um número válido.'],
            ['column' => 'number', 'value' => 'fsdf', 'message' => 'Forneça um número válido.'],
            ['column' => 'complement', 'value' => Str::random(16), 'message' => 'Limite o campo a no máximo 15 caracteres.'],
            ['column' => 'cep', 'value' => 'fsdf', 'message' => 'Formato de cep aceito 99.999-99/99999-999.'],
            ['column' => 'latitude', 'value' => 'fsdf', 'message' => 'Forneça um número válido.'],
            ['column' => 'longitude', 'value' => 'fsdf', 'message' => 'Forneça um número válido.'],
            ['column' => 'neighborhood_id', 'value' => '', 'message' => 'Escolha um bairro para o endereço.'],
            ['column' => 'neighborhood_id', 'value' => Str::uuid(), 'message' => 'Escolha um id de bairro válido.'],
        ];
        $response = $this->postJson($this->baseUrl, [
            'neighborhood_id' => $neighborhood->id,
            'address' => 'Rua Amazonas',
            'not_number' => true,
            'complement' => 'casa 2',
            'cep' => '37701-234',
            'latitude' => -39,
            'longitude' => 69,
        ]);
        $response->assertStatus(201);

        foreach ($validation as $key => $value) {
            $aux = $dataRequest;
            $aux[$value['column']] = $value['value'];
            $response = $this->postJson($this->baseUrl, $aux);
            $response->assertStatus(422);
            $this->assertCount(1, $response->json('errors'));
            $this->assertEquals($value['message'], $response->json("errors.{$value['column']}.0"));
        }
        $address = Address::first();
        $response = $this->patchJson("{$this->baseUrl}/{$address->id}", ['neighborhood_id' => $neighborhood->id, 'address' => 'JD Horizonte', 'not_number' => true]);
        $response->assertStatus(200);
    }

    /**
     * @group address
     * @group validate
     * @group address-validate
     */
    public function testValidateCascade()
    {
        $country = factory(Country::class)->create();
        $state = factory(State::class)->create(['country_id' => $country->id]);
        $city = factory(City::class)->create(['state_id' => $state->id]);
        $neighborhood = factory(Neighborhood::class)->create(['city_id' => $city->id]);
        $dataRequest = [
            'initials' => 'MG',
            'state' => 'Minas Gerais',
            'city' => 'Belo Horizonte',
            'neighborhood' => 'Alagoas',
            'address' => 'Rua Amazonas',
            'number' => 23,
            'complement' => 'casa 2',
            'cep' => '37701-234',
            'latitude' => -39,
            'longitude' => 69,
        ];
        $validation = [
            ['column' => 'address', 'value' => Str::random(101), 'message' => 'Limite o campo a no máximo 100 caracteres.'],
            ['column' => 'number', 'value' => -1, 'message' => 'Forneça um número válido.'],
            ['column' => 'number', 'value' => 'fsdf', 'message' => 'Forneça um número válido.'],
            ['column' => 'complement', 'value' => Str::random(16), 'message' => 'Limite o campo a no máximo 15 caracteres.'],
            ['column' => 'cep', 'value' => 'fsdf', 'message' => 'Formato de cep aceito 99.999-99/99999-999.'],
            ['column' => 'latitude', 'value' => 'fsdf', 'message' => 'Forneça um número válido.'],
            ['column' => 'longitude', 'value' => 'fsdf', 'message' => 'Forneça um número válido.'],
            ['column' => 'neighborhood', 'value' => '', 'message' => 'Forneça um bairro para o endereço.'],
            ['column' => 'neighborhood', 'value' => Str::random(101), 'message' => 'Limite o campo a no máximo 100 caracteres.'],
            ['column' => 'city', 'value' => '', 'message' => 'Forneça uma cidade para o endereço.'],
            ['column' => 'city', 'value' => Str::random(101), 'message' => 'Limite o campo a no máximo 100 caracteres.'],
            ['column' => 'state', 'value' => Str::random(101), 'message' => 'Limite o campo a no máximo 100 caracteres.'],
            ['column' => 'state', 'value' => '', 'message' => 'Forneça um estado para o endereço.'],
            ['column' => 'initials', 'value' => '', 'message' => 'Forneça a sigla do estado para o endereço.'],
            ['column' => 'initials', 'value' => Str::random(3), 'message' => 'Limite o campo a no máximo 2 caracteres.'],
            ['column' => 'country', 'value' => Str::random(101), 'message' => 'Limite o campo a no máximo 100 caracteres.'],
        ];
        $response = $this->postJson("{$this->baseUrl}-cascade", [
            'initials' => 'SP',
            'state' => 'São Paulo',
            'city' => 'Americana',
            'neighborhood' => 'Centro',
            'address' => 'Rua Amazonas',
            'not_number' => true,
            'complement' => 'casa 2',
            'cep' => '37701-234',
            'latitude' => -39,
            'longitude' => 69,
        ]);
        $response->assertStatus(201);

        foreach ($validation as $key => $value) {
            $aux = $dataRequest;
            $aux[$value['column']] = $value['value'];
            $response = $this->postJson("{$this->baseUrl}-cascade", $aux);
            $response->assertStatus(422);
            $this->assertCount(1, $response->json('errors'));
            $this->assertEquals($value['message'], $response->json("errors.{$value['column']}.0"));
        }
        $address = Address::first();
        $response = $this->patchJson("{$this->baseUrl}/{$address->id}", ['neighborhood_id' => $neighborhood->id, 'address' => 'JD Horizonte', 'not_number' => true]);
        $response->assertStatus(200);
    }

    /**
     * @group address
     * @group index
     * @group address-index
     */
    public function testIndex()
    {
        $country = factory(Country::class)->create();
        $state = factory(State::class)->create(['country_id' => $country->id, 'name' => 'Minas Gerais']);
        $city = factory(City::class)->create(['state_id' => $state->id]);
        $neighborhood = factory(Neighborhood::class)->create(['city_id' => $city->id]);
        $neighborhoodCg = factory(Neighborhood::class)->create(['city_id' => $city->id, 'name' => 'Campo Grande']);
        factory(Address::class, 5)->create(['neighborhood_id' => $neighborhood->id]);
        factory(Address::class)->create(['neighborhood_id' => $neighborhoodCg->id, 'address' => 'Av vicentina']);
        factory(Address::class)->create(['neighborhood_id' => $neighborhoodCg->id, 'address' => 'Rua Alagoas']);

        $addresses = Address::all();
        $response = $this->getJson($this->baseUrl);
        $response->assertStatus(200);
        $dataResult = ['data' => [], 'meta' => [
            'current_page' => 1,
            'from' => 1,
            'last_page' => 1,
            'links' => [
                [
                    'url' => null,
                    'label' => '&laquo; Previous',
                    'active' => false,
                ],
                [
                    'url' => 'http://localhost/api/address?page=1',
                    'label' => '1',
                    'active' => true,
                ],
                [
                    'url' => null,
                    'label' => 'Next &raquo;',
                    'active' => false,
                ],
            ],
            'path' => 'http://localhost/api/address',
            'per_page' => -1,
            'to' => 7,
            'total' => 7,
        ], 'links' => [
            'first' => 'http://localhost/api/address?page=1',
            'last' => 'http://localhost/api/address?page=1',
            'prev' => null,
            'next' => null,
        ], 'included' => [
            ['type' => 'neighborhood', 'id' => $neighborhood->id, 'attributes' => ['slug' => $neighborhood->slug, 'name' => Str::title($neighborhood->name), 'created_at' => $neighborhood->created_at], 'relationships' => ['city' => ['data' => ['type' => 'city', 'id' => $neighborhood->city_id]]]],
            ['type' => 'neighborhood', 'id' => $neighborhoodCg->id, 'attributes' => ['slug' => $neighborhoodCg->slug, 'name' => Str::title($neighborhoodCg->name), 'created_at' => $neighborhoodCg->created_at], 'relationships' => ['city' => ['data' => ['type' => 'city', 'id' => $neighborhoodCg->city_id]]]],
        ]];
        $dataResult['data'] = array_values($addresses->map(function ($address) {
            return [
                'type' => 'address',
                'id' => $address->id,
                'attributes' => [
                    'address' => $address->address,
                    'number' => $address->number,
                    'not_number' => $address->not_number ? true : false,
                    'complement' => $address->complement,
                    'cep' => $address->cep,
                    'latitude' => number_format($address->latitude, 2, '.', ''),
                    'longitude' => number_format($address->longitude, 2, '.', ''),
                    'created_at' => $address->created_at->format('Y-m-d H:i:s'),
                ],
                'relationships' => [
                    'neighborhood' => [
                        'data' => [
                            'id' => $address->neighborhood_id,
                            'type' => 'neighborhood',
                        ],
                    ],
                ],
            ];
        })->toArray());
        $dataApi = json_decode($response->getContent(), true);
        $this->assertEquals(2, count($dataApi['included']));
        $this->assertEqualsCanonicalizing($dataResult, $dataApi);
        $this->assertEquals(7, count($dataApi['data']));

        // Test search
        $addressChain = Address::where('address', 'like', 'Rua Alagoas')->first();
        $response = $this->json('GET', $this->baseUrl, ['search' => 'Rua Alagoas']);
        $dataApi = json_decode($response->getContent(), true);
        $this->assertEquals(1, count($dataApi['data']));
        $this->assertEqualsCanonicalizing([
            [
                'type' => 'address', 'id' => $addressChain->id, 'attributes' => [
                    'address' => $addressChain->address,
                    'number' => $addressChain->number,
                    'not_number' => $addressChain->not_number ? true : false,
                    'complement' => $addressChain->complement,
                    'cep' => $addressChain->cep,
                    'latitude' => number_format($addressChain->latitude, 2, '.', ''),
                    'longitude' => number_format($addressChain->longitude, 2, '.', ''),
                    'created_at' => $addressChain->created_at->format('Y-m-d H:i:s'),
                ], 'relationships' => [
                    'neighborhood' => ['data' => ['id' => $addressChain->neighborhood_id, 'type' => 'neighborhood']],
                ],
            ],
        ], $dataApi['data']);
        $this->assertEqualsCanonicalizing(
            [
                [
                    'type' => 'neighborhood',
                    'id' => $addressChain->neighborhood_id,
                    'attributes' => ['name' => Str::title($addressChain->neighborhood->name), 'slug' => $addressChain->neighborhood->slug, 'created_at' => $addressChain->neighborhood->created_at->format('Y-m-d H:i:s')],
                    'relationships' => ['city' => ['data' => ['type' => 'city', 'id' => $addressChain->neighborhood->city_id]]],
                ],
            ],
            $dataApi['included']
        );

        // Test paginate
        $response = $this->json('GET', $this->baseUrl, ['paginate' => 2]);
        $dataApi = json_decode($response->getContent(), true);
        $this->assertEquals(2, count($dataApi['data']));

        // Test order
        $dataNews = ['a', 'b', 'c', 'd', 'e'];
        Address::where('address', '!=', '')->delete();
        $this->assertEquals(0, Address::count());
        foreach ($dataNews as $value) {
            factory(Address::class)->create(['neighborhood_id' => $neighborhood->id, 'address' => $value]);
        }
        $response = $this->json('GET', $this->baseUrl, ['order' => 'asc', 'column' => 'address']);
        $dataApi = json_decode($response->getContent(), true);
        foreach ($dataApi['data'] as $key => $state) {
            $this->assertEquals($dataNews[$key], $state['attributes']['address']);
        }
        $response = $this->json('GET', $this->baseUrl, ['order' => 'desc', 'column' => 'address']);
        $dataApi = json_decode($response->getContent(), true);
        $i = 4;
        foreach ($dataApi['data'] as $key => $state) {
            $this->assertEquals($dataNews[$i], $state['attributes']['address']);
            $i--;
        }
    }

    /**
     * @group address
     * @group relationships
     * @group address-relationships
     */
    public function testRelationships()
    {
        $country = factory(Country::class)->create();
        $state = factory(State::class)->create(['country_id' => $country->id]);
        $city = factory(City::class)->create(['state_id' => $state->id]);
        $neighborhood = factory(Neighborhood::class)->create(['city_id' => $city->id]);
        $address = factory(Address::class)->create(['neighborhood_id' => $neighborhood->id]);
        $response = $this->getJson("{$this->baseUrl}/{$address->id}/relationships/neighborhood");
        $response->assertStatus(200);
        $dataApi = json_decode($response->getContent(), true);
        $this->assertEquals([
            'type' => 'neighborhood',
            'id' => $neighborhood->id,
            'attributes' => [
                'slug' => $neighborhood->slug,
                'name' => Str::title($neighborhood->name),
                'created_at' => $neighborhood->created_at->format('Y-m-d H:i:s'),
            ], 'relationships' => ['city' => ['data' => ['type' => 'city', 'id' => $neighborhood->city_id]]],
        ], $dataApi['data']);
    }

    /**
     * @group address
     * @group show
     * @group address-show
     */
    public function testShow()
    {
        $country = factory(Country::class)->create();
        $state = factory(State::class)->create(['country_id' => $country->id]);
        $city = factory(City::class)->create(['state_id' => $state->id]);
        $neighborhood = factory(Neighborhood::class)->create(['city_id' => $city->id]);
        $address = factory(Address::class)->create(['neighborhood_id' => $neighborhood->id]);
        $response = $this->getJson("{$this->baseUrl}/{$address->id}");
        $response->assertStatus(200);
        $dataApi = json_decode($response->getContent(), true);
        $this->assertEqualsCanonicalizing([
            [
                'type' => 'neighborhood', 'id' => $neighborhood->id,
                'attributes' => [
                    'name' => Str::title($neighborhood->name),
                    'slug' => $neighborhood->slug,
                    'created_at' => $address->created_at->format('Y-m-d H:i:s'),
                ],
                'relationships' => ['city' => ['data' => ['type' => 'city', 'id' => $city->id]]],
            ],
        ], $dataApi['included']);
        $this->assertEquals([
            'type' => 'address',
            'id' => $address->id,
            'attributes' => [
                'address' => $address->address,
                'number' => $address->number,
                'not_number' => $address->not_number ? true : false,
                'complement' => $address->complement,
                'cep' => $address->cep,
                'latitude' => number_format(round($address->latitude), 2, '.', ''),
                'longitude' => number_format(round($address->longitude), 2, '.', ''),
                'created_at' => $address->created_at->format('Y-m-d H:i:s'),
            ],
            'relationships' => [
                'neighborhood' => [
                    'data' => [
                        'id' => $address->neighborhood_id,
                        'type' => 'neighborhood',
                    ],
                ],
            ],
        ], $dataApi['data']);
    }

    /**
     * @group address
     * @group store
     * @group address-store
     */
    public function testStore()
    {
        $this->assertNull(Address::first());
        $country = factory(Country::class)->create();
        $state = factory(State::class)->create(['country_id' => $country->id]);
        $city = factory(City::class)->create(['state_id' => $state->id]);
        $neighborhood = factory(Neighborhood::class)->create(['city_id' => $city->id]);

        $response = $this->postJson($this->baseUrl, [
            'neighborhood_id' => $neighborhood->id,
            'address' => 'Rua Amazonas',
            'number' => 23,
            'complement' => 'casa 2',
            'cep' => '37701-234',
            'latitude' => -39,
            'longitude' => 69,
        ]);
        $response->assertStatus(201);
        $address = Address::first();
        $this->assertNotNull($address);
        $this->assertEquals($address->address, 'Rua Amazonas');
        $this->assertEquals($address->number, 23);
        $this->assertEquals((bool) $address->not_number, false);
        $this->assertEquals($address->complement, 'casa 2');
        $this->assertEquals($address->cep, '37701234');
        $this->assertEquals($address->latitude, -39);
        $this->assertEquals($address->longitude, 69);
        $this->assertEquals($address->neighborhood_id, $neighborhood->id);
    }

    /**
     * @group address
     * @group store
     * @group address-store
     */
    public function testStoreWithNotNumber()
    {
        $this->assertNull(Address::first());
        $country = factory(Country::class)->create();
        $state = factory(State::class)->create(['country_id' => $country->id]);
        $city = factory(City::class)->create(['state_id' => $state->id]);
        $neighborhood = factory(Neighborhood::class)->create(['city_id' => $city->id]);

        $response = $this->postJson($this->baseUrl, [
            'neighborhood_id' => $neighborhood->id,
            'address' => 'Rua Amazonas',
            'number' => 23,
            'not_number' => true,
            'complement' => 'casa 2',
            'cep' => '37701-234',
            'latitude' => -39,
            'longitude' => 69,
        ]);
        $response->assertStatus(201);
        $address = Address::first();
        $this->assertNotNull($address);
        $this->assertEquals($address->address, 'Rua Amazonas');
        $this->assertEquals($address->number, null);
        $this->assertEquals($address->not_number, true);
        $this->assertEquals($address->complement, 'casa 2');
        $this->assertEquals($address->cep, '37701234');
        $this->assertEquals($address->latitude, -39);
        $this->assertEquals($address->longitude, 69);
        $this->assertEquals($address->neighborhood_id, $neighborhood->id);
    }

    /**
     * @group address
     * @group store
     * @group address-store
     * @group address-store-cascade
     */
    public function testStoreCascade()
    {
        $this->assertNull(Address::first());

        $response = $this->postJson("{$this->baseUrl}-cascade", [
            'country' => 'Test de país',
            'initials' => 'TS',
            'state' => 'Test de estado',
            'city' => 'Test de cidade',
            'neighborhood' => 'Test de bairro',
            'address' => 'Rua Amazonas',
            'number' => 23,
            'complement' => 'casa 2',
            'cep' => '37701-234',
            'latitude' => -39,
            'longitude' => 69,
        ]);
        $response->assertStatus(201);
        $address = Address::first();
        $response->assertJsonStructure([
            'data' => ['type', 'id', 'attributes' => ['address', 'number', 'not_number', 'complement', 'cep', 'latitude', 'longitude', 'created_at']],
            'included' => [
                ['type', 'id', 'attributes' => ['slug', 'name', 'created_at'], 'relationships' => ['city']],
            ],
        ]);
        $this->assertNotNull($address);
        $this->assertEquals($address->address, 'Rua Amazonas');
        $this->assertEquals($address->number, 23);
        $this->assertEquals((bool) $address->not_number, false);
        $this->assertEquals($address->complement, 'casa 2');
        $this->assertEquals($address->cep, '37701234');
        $this->assertEquals($address->latitude, -39);
        $this->assertEquals($address->longitude, 69);
        $this->assertEquals($address->neighborhood->name, Str::upper('Test de bairro'));
        $this->assertEquals($address->neighborhood->city->name, Str::upper('Test de cidade'));
        $this->assertEquals($address->neighborhood->city->state->name, Str::upper('Test de estado'));
        $this->assertEquals($address->neighborhood->city->state->initials, 'TS');
        $this->assertEquals($address->neighborhood->city->state->country->name, Str::upper('Test de país'));
    }

    /**
     * @group address
     * @group update
     * @group address-update
     */
    public function testUpdate()
    {
        $country = factory(Country::class)->create();
        $state = factory(State::class)->create(['country_id' => $country->id]);
        $city = factory(City::class)->create(['state_id' => $state->id]);
        $neighborhood = factory(Neighborhood::class)->create(['city_id' => $city->id, 'name' => 'Curvelo']);
        $address = factory(Address::class)->create(['neighborhood_id' => $neighborhood->id]);
        $this->assertNotNull(Address::first());

        $response = $this->patchJson("{$this->baseUrl}/{$address->id}", [
            'neighborhood_id' => $neighborhood->id,
            'address' => 'Rua Alagoas',
            'number' => 23,
            'complement' => 'casa 2',
            'cep' => '37701-234',
            'latitude' => -39,
            'longitude' => 69,
        ]);
        $response->assertStatus(200);
        $address = Address::first();
        $this->assertNotNull($address);
        $this->assertEquals($address->address, 'Rua Alagoas');
        $this->assertEquals($address->number, 23);
        $this->assertEquals((bool) $address->not_number, false);
        $this->assertEquals($address->complement, 'casa 2');
        $this->assertEquals($address->cep, '37701234');
        $this->assertEquals($address->latitude, -39);
        $this->assertEquals($address->longitude, 69);
        $this->assertEquals($address->neighborhood_id, $neighborhood->id);
    }

    /**
     * @group address
     * @group update
     * @group address-update
     */
    public function testUpdateCascade()
    {
        $country = factory(Country::class)->create();
        $state = factory(State::class)->create(['country_id' => $country->id]);
        $city = factory(City::class)->create(['state_id' => $state->id]);
        $neighborhood = factory(Neighborhood::class)->create(['city_id' => $city->id, 'name' => 'Curvelo']);
        $address = factory(Address::class)->create(['neighborhood_id' => $neighborhood->id]);
        $this->assertNotNull(Address::first());

        $response = $this->patchJson("{$this->baseUrl}-cascade/{$address->id}", [
            'neighborhood' => 'Centro',
            'city' => 'Americana',
            'initials' => 'MG',
            'state' => 'Minas Gerais',
            'country' => 'Canadá',
            'address' => 'Rua Alagoas',
            'number' => 23,
            'complement' => 'casa 2',
            'cep' => '37701-234',
            'latitude' => -39,
            'longitude' => 69,
        ]);
        $response->assertStatus(200);
        $address = Address::first();
        $this->assertNotNull($address);
        $this->assertEquals($address->address, 'Rua Alagoas');
        $this->assertEquals($address->number, 23);
        $this->assertEquals((bool) $address->not_number, false);
        $this->assertEquals($address->complement, 'casa 2');
        $this->assertEquals($address->cep, '37701234');
        $this->assertEquals($address->latitude, -39);
        $this->assertEquals($address->longitude, 69);
        $this->assertEquals($address->neighborhood->name, 'CENTRO');
        $this->assertEquals($address->neighborhood->city->name, 'AMERICANA');
        $this->assertEquals($address->neighborhood->city->state->name, 'MINAS GERAIS');
        $this->assertEquals($address->neighborhood->city->state->initials, 'MG');
        $this->assertEquals($address->neighborhood->city->state->country->name, 'CANADÁ');
    }

    /**
     * @group address
     * @group update
     * @group address-update
     */
    public function testUpdateCascadeNeighborhood()
    {
        $country = factory(Country::class)->create();
        $state = factory(State::class)->create(['country_id' => $country->id]);
        $city = factory(City::class)->create(['state_id' => $state->id]);
        $neighborhood = factory(Neighborhood::class)->create(['city_id' => $city->id, 'name' => 'Curvelo']);
        $address = factory(Address::class)->create(['neighborhood_id' => $neighborhood->id]);
        $this->assertNotNull(Address::first());

        $response = $this->patchJson("{$this->baseUrl}-cascade/{$address->id}", [
            'neighborhood' => 'Centro',
            'address' => 'Rua Alagoas',
            'number' => 23,
            'complement' => 'casa 2',
            'cep' => '37701-234',
            'latitude' => -39,
            'longitude' => 69,
        ]);
        $response->assertStatus(200);
        $address = Address::first();
        $this->assertNotNull($address);
        $this->assertEquals($address->address, 'Rua Alagoas');
        $this->assertEquals($address->number, 23);
        $this->assertEquals((bool) $address->not_number, false);
        $this->assertEquals($address->complement, 'casa 2');
        $this->assertEquals($address->cep, '37701234');
        $this->assertEquals($address->latitude, -39);
        $this->assertEquals($address->longitude, 69);
        $this->assertEquals($address->neighborhood->name, 'CENTRO');
        $this->assertEquals($address->neighborhood->city->name, $city->name);
        $this->assertEquals($address->neighborhood->city->state->name, $state->name);
        $this->assertEquals($address->neighborhood->city->state->initials, $state->initials);
        $this->assertEquals($address->neighborhood->city->state->country->name, $country->name);
    }

    /**
     * @group address
     * @group update
     * @group address-update
     */
    public function testUpdateCascadeCity()
    {
        $country = factory(Country::class)->create();
        $state = factory(State::class)->create(['country_id' => $country->id]);
        $city = factory(City::class)->create(['state_id' => $state->id]);
        $neighborhood = factory(Neighborhood::class)->create(['city_id' => $city->id, 'name' => 'Curvelo']);
        $address = factory(Address::class)->create(['neighborhood_id' => $neighborhood->id]);
        $this->assertNotNull(Address::first());

        $response = $this->patchJson("{$this->baseUrl}-cascade/{$address->id}", [
            'city' => 'Americana',
            'address' => 'Rua Alagoas',
            'number' => 23,
            'complement' => 'casa 2',
            'cep' => '37701-234',
            'latitude' => -39,
            'longitude' => 69,
        ]);
        $response->assertStatus(200);
        $address = Address::first();
        $this->assertNotNull($address);
        $this->assertEquals($address->address, 'Rua Alagoas');
        $this->assertEquals($address->number, 23);
        $this->assertEquals((bool) $address->not_number, false);
        $this->assertEquals($address->complement, 'casa 2');
        $this->assertEquals($address->cep, '37701234');
        $this->assertEquals($address->latitude, -39);
        $this->assertEquals($address->longitude, 69);
        $this->assertEquals($address->neighborhood->name, $neighborhood->name);
        $this->assertEquals($address->neighborhood->city->name, 'AMERICANA');
        $this->assertEquals($address->neighborhood->city->state->name, $state->name);
        $this->assertEquals($address->neighborhood->city->state->initials, $state->initials);
        $this->assertEquals($address->neighborhood->city->state->country->name, $country->name);
    }

    /**
     * @group address
     * @group update
     * @group address-update
     */
    public function testUpdateCascadeState()
    {
        $country = factory(Country::class)->create();
        $state = factory(State::class)->create(['country_id' => $country->id]);
        $city = factory(City::class)->create(['state_id' => $state->id]);
        $neighborhood = factory(Neighborhood::class)->create(['city_id' => $city->id, 'name' => 'Curvelo']);
        $address = factory(Address::class)->create(['neighborhood_id' => $neighborhood->id]);
        $this->assertNotNull(Address::first());

        $response = $this->patchJson("{$this->baseUrl}-cascade/{$address->id}", [
            'initials' => 'MG',
            'state' => 'Minas Gerais',
            'address' => 'Rua Alagoas',
            'number' => 23,
            'complement' => 'casa 2',
            'cep' => '37701-234',
            'latitude' => -39,
            'longitude' => 69,
        ]);
        $response->assertStatus(200);
        $address = Address::first();
        $this->assertNotNull($address);
        $this->assertEquals($address->address, 'Rua Alagoas');
        $this->assertEquals($address->number, 23);
        $this->assertEquals((bool) $address->not_number, false);
        $this->assertEquals($address->complement, 'casa 2');
        $this->assertEquals($address->cep, '37701234');
        $this->assertEquals($address->latitude, -39);
        $this->assertEquals($address->longitude, 69);
        $this->assertEquals($address->neighborhood->name, $neighborhood->name);
        $this->assertEquals($address->neighborhood->city->name, $city->name);
        $this->assertEquals($address->neighborhood->city->state->name, 'MINAS GERAIS');
        $this->assertEquals($address->neighborhood->city->state->initials, 'MG');
        $this->assertEquals($address->neighborhood->city->state->country->name, $country->name);
    }

    /**
     * @group address
     * @group update
     * @group address-update
     */
    public function testUpdateCascadeCountry()
    {
        $country = factory(Country::class)->create();
        $state = factory(State::class)->create(['country_id' => $country->id]);
        $city = factory(City::class)->create(['state_id' => $state->id]);
        $neighborhood = factory(Neighborhood::class)->create(['city_id' => $city->id, 'name' => 'Curvelo']);
        $address = factory(Address::class)->create(['neighborhood_id' => $neighborhood->id]);
        $this->assertNotNull(Address::first());

        $response = $this->patchJson("{$this->baseUrl}-cascade/{$address->id}", [
            'country' => 'Canadá',
            'address' => 'Rua Alagoas',
            'number' => 23,
            'complement' => 'casa 2',
            'cep' => '37701-234',
            'latitude' => -39,
            'longitude' => 69,
        ]);
        $response->assertStatus(200);
        $address = Address::first();
        $this->assertNotNull($address);
        $this->assertEquals($address->address, 'Rua Alagoas');
        $this->assertEquals($address->number, 23);
        $this->assertEquals((bool) $address->not_number, false);
        $this->assertEquals($address->complement, 'casa 2');
        $this->assertEquals($address->cep, '37701234');
        $this->assertEquals($address->latitude, -39);
        $this->assertEquals($address->longitude, 69);
        $this->assertEquals($address->neighborhood->name, $neighborhood->name);
        $this->assertEquals($address->neighborhood->city->name, $city->name);
        $this->assertEquals($address->neighborhood->city->state->name, $state->name);
        $this->assertEquals($address->neighborhood->city->state->initials, $state->initials);
        $this->assertEquals($address->neighborhood->city->state->country->name, 'CANADÁ');
    }

    /**
     * @group address
     * @group destroy
     * @group address-destroy
     */
    public function testDestroy()
    {
        $country = factory(Country::class)->create();
        $state = factory(State::class)->create(['country_id' => $country->id]);
        $city = factory(City::class)->create(['state_id' => $state->id]);
        $neighborhood = factory(Neighborhood::class)->create(['city_id' => $city->id]);
        $address = factory(Address::class)->create(['neighborhood_id' => $neighborhood->id]);
        $this->assertNotNull(Address::first());
        $response = $this->deleteJson("{$this->baseUrl}/{$address->id}");
        $response->assertStatus(200);
        $this->assertNull(Address::first());
    }
}
