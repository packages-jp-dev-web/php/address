<?php

namespace JPAddress\Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use JPAddress\Tests\TestCase;
use Illuminate\Support\Str;
// Models
use JPAddress\Models\Address\State;
use JPAddress\Models\Address\Country;
use JPAddress\Models\Address\City;
use JPAddress\Models\Address\Neighborhood;

// When testing inside of a Laravel installation, the base class would be Tests\TestCase
class NeighborhoodTest extends TestCase
{
    use RefreshDatabase;
    protected $baseUrl = '/api/neighborhood';

    // Use annotation @test so that PHPUnit knows about the test

    /** @test
     * @group neighborhood
     * @group validate
     * @group neighborhood-validate
     */
    public function validate()
    {
        $country = factory(Country::class)->create();
        $state = factory(State::class)->create(['country_id' => $country->id]);
        $city = factory(City::class)->create(['state_id' => $state->id]);
        $dataRequest = ['name' => 'Qualquer', 'city_id' => $city->id];
        $validation = [
            ['column' => 'name', 'value' => ''],
            ['column' => 'name', 'value' => Str::random(101)],
            ['column' => 'name', 'value' => 'JD Horizonte'],
            ['column' => 'city_id', 'value' => ''],
            ['column' => 'city_id', 'value' => 'lklkjj'],
        ];
        $response = $this->postJson($this->baseUrl, [
            'city_id' => $city->id,
            'name' => 'JD Horizonte',
        ]);
        $response->assertStatus(201);

        foreach ($validation as $key => $value) {
            $aux = $dataRequest;
            $aux[$value['column']] = $value['value'];
            $response = $this->postJson($this->baseUrl, $aux);
            $response->assertStatus(422);
        }
        $neighborhood = Neighborhood::first();
        $response = $this->patchJson("{$this->baseUrl}/{$neighborhood->id}", ['city_id' => $city->id, 'name' => 'JD Horizonte']);
        $response->assertStatus(200);
    }

    /** @test
     * @group neighborhood
     * @group index
     * @group neighborhood-index
     */
    public function index()
    {
        $country = factory(Country::class)->create();
        $state = factory(State::class)->create(['country_id' => $country->id, 'name' => 'Minas Gerais']);
        $city = factory(City::class)->create(['state_id' => $state->id]);
        $cityCg = factory(City::class)->create(['state_id' => $state->id, 'name' => 'campos gerais']);
        factory(Neighborhood::class, 5)->create(['city_id' => $city->id]);
        factory(Neighborhood::class)->create(['city_id' => $cityCg->id, 'name' => 'Caldas']);
        factory(Neighborhood::class)->create(['city_id' => $cityCg->id, 'name' => 'Campo Grande']);

        $neighborhoods = Neighborhood::all();
        $response = $this->getJson($this->baseUrl);
        $response->assertStatus(200);
        $dataResult = ['data' => [], 'meta' => [
            'current_page' => 1,
            'from' => 1,
            'last_page' => 1,
            'links' => [
                [
                    'url' => null,
                    'label' => '&laquo; Previous',
                    'active' => false,
                ],
                [
                    'url' => 'http://localhost/api/neighborhood?page=1',
                    'label' => '1',
                    'active' => true,
                ],
                [
                    'url' => null,
                    'label' => 'Next &raquo;',
                    'active' => false,
                ],
            ],
            'path' => 'http://localhost/api/neighborhood',
            'per_page' => -1,
            'to' => 7,
            'total' => 7,
        ], 'links' => [
            'first' => 'http://localhost/api/neighborhood?page=1',
            'last' => 'http://localhost/api/neighborhood?page=1',
            'prev' => null,
            'next' => null,
        ], 'included' => [
            ['type' => 'city', 'id' => $city->id, 'attributes' => ['slug' => $city->slug, 'name' => Str::title($city->name), 'created_at' => $city->created_at->format('Y-m-d H:i:s')], 'relationships' => ['state' => ['data' => ['type' => 'state', 'id' => $city->state_id]]]],
            ['type' => 'city', 'id' => $cityCg->id, 'attributes' => ['slug' => $cityCg->slug, 'name' => Str::title($cityCg->name), 'created_at' => $cityCg->created_at->format('Y-m-d H:i:s')], 'relationships' => ['state' => ['data' => ['type' => 'state', 'id' => $cityCg->state_id]]]],
        ]];
        $dataResult['data'] = array_values($neighborhoods->map(function ($neighborhood) {
            return [
                'type' => 'neighborhood',
                'id' => $neighborhood->id,
                'attributes' => [
                    'name' => Str::title($neighborhood->name),
                    'slug' => $neighborhood->slug,
                    'created_at' => $neighborhood->created_at->format('Y-m-d H:i:s'),
                ],
                'relationships' => [
                    'city' => ['data' => ['id' => $neighborhood->city_id, 'type' => 'city']],
                ],
            ];
        })->toArray());
        $dataApi = json_decode($response->getContent(), true);
        $this->assertEquals(2, count($dataApi['included']));
        $this->assertEqualsCanonicalizing($dataResult, $dataApi);
        $this->assertEquals(7, count($dataApi['data']));

        // Test search
        $neighborhoodChain = Neighborhood::where('name', 'like', 'Campo Grande')->first();
        $response = $this->json('GET', $this->baseUrl, ['search' => 'Campo Grande']);
        $dataApi = json_decode($response->getContent(), true);
        $this->assertEquals(1, count($dataApi['data']));
        $this->assertEqualsCanonicalizing([
            [
                'type' => 'neighborhood', 'id' => $neighborhoodChain->id, 'attributes' => [
                    'name' => Str::title($neighborhoodChain->name),
                    'slug' => $neighborhoodChain->slug, 'created_at' => $neighborhoodChain->created_at->format('Y-m-d H:i:s'),
                ], 'relationships' => [
                    'city' => ['data' => ['id' => $neighborhoodChain->city_id, 'type' => 'city']],
                ],
            ],
        ], $dataApi['data']);
        $this->assertEqualsCanonicalizing(
            [
                [
                    'type' => 'city',
                    'id' => $neighborhoodChain->city_id,
                    'attributes' => ['name' => Str::title($neighborhoodChain->city->name), 'slug' => $neighborhoodChain->city->slug, 'created_at' => $neighborhoodChain->created_at->format('Y-m-d H:i:s')],
                    'relationships' => ['state' => ['data' => ['type' => 'state', 'id' => $neighborhoodChain->city->state_id]]],
                ],
            ],
            $dataApi['included']
        );

        // Test paginate
        $response = $this->json('GET', $this->baseUrl, ['paginate' => 2]);
        $dataApi = json_decode($response->getContent(), true);
        $this->assertEquals(2, count($dataApi['data']));

        // Test order
        $dataNews = ['a', 'b', 'c', 'd', 'e'];
        Neighborhood::where('name', '!=', '')->delete();
        $this->assertEquals(0, Neighborhood::count());
        foreach ($dataNews as $value) {
            Neighborhood::create(['city_id' => $city->id, 'name' => $value]);
        }
        $response = $this->json('GET', $this->baseUrl, ['order' => 'asc', 'column' => 'name']);
        $dataApi = json_decode($response->getContent(), true);
        foreach ($dataApi['data'] as $key => $state) {
            $this->assertEquals($dataNews[$key], $state['attributes']['slug']);
        }
        $response = $this->json('GET', $this->baseUrl, ['order' => 'desc', 'column' => 'name']);
        $dataApi = json_decode($response->getContent(), true);
        $i = 4;
        foreach ($dataApi['data'] as $key => $state) {
            $this->assertEquals($dataNews[$i], $state['attributes']['slug']);
            $i--;
        }
    }

    /** @test
     * @group neighborhood
     * @group relationships
     * @group neighborhood-relationships
     */
    public function relationships()
    {
        $country = factory(Country::class)->create();
        $state = factory(State::class)->create(['country_id' => $country->id]);
        $city = factory(City::class)->create(['state_id' => $state->id]);
        $neighborhood = factory(Neighborhood::class)->create(['city_id' => $city->id]);
        $response = $this->getJson("{$this->baseUrl}/{$neighborhood->id}/relationships/city");
        $response->assertStatus(200);
        $dataApi = json_decode($response->getContent(), true);
        $this->assertEquals([
            'type' => 'city',
            'id' => $city->id,
            'attributes' => [
                'slug' => $city->slug,
                'name' => Str::title($city->name),
                'created_at' => $city->created_at->format('Y-m-d H:i:s'),
            ], 'relationships' => ['state' => ['data' => ['type' => 'state', 'id' => $city->state_id]]],
        ], $dataApi['data']);
    }

    /** @test
     * @group neighborhood
     * @group show
     * @group neighborhood-show
     */
    public function show()
    {
        $country = factory(Country::class)->create();
        $state = factory(State::class)->create(['country_id' => $country->id]);
        $city = factory(City::class)->create(['state_id' => $state->id]);
        $neighborhood = factory(Neighborhood::class)->create(['city_id' => $city->id]);
        $response = $this->getJson("{$this->baseUrl}/{$neighborhood->id}");
        $response->assertStatus(200);
        $dataApi = json_decode($response->getContent(), true);
        $this->assertEqualsCanonicalizing([
            [
                'type' => 'city', 'id' => $city->id,
                'attributes' => [
                    'name' => Str::title($city->name), 'slug' => $city->slug,
                    'created_at' => $city->created_at->format('Y-m-d H:i:s'),
                ],
                'relationships' => ['state' => ['data' => ['type' => 'state', 'id' => $state->id]]],
            ],
        ], $dataApi['included']);
        $this->assertEqualsCanonicalizing([
            'type' => 'neighborhood',
            'id' => $neighborhood->id,
            'attributes' => [
                'name' => Str::title($neighborhood->name),
                'slug' => $neighborhood->slug,
                'created_at' => $neighborhood->created_at->format('Y-m-d H:i:s'),
            ],
            'relationships' => [
                'city' => [
                    'data' => [
                        'type' => 'city',
                        'id' => $neighborhood->city_id,
                    ],
                ],
            ],
        ], $dataApi['data']);
    }

    /** @test
     * @group neighborhood
     * @group store
     * @group neighborhood-store
     */
    public function store()
    {
        $this->assertNull(Neighborhood::first());
        $country = factory(Country::class)->create();
        $state = factory(State::class)->create(['country_id' => $country->id]);
        $city = factory(City::class)->create(['state_id' => $state->id]);
        $response = $this->postJson($this->baseUrl, [
            'city_id' => $city->id,
            'name' => 'Jd Azaleia',
        ]);
        $response->assertStatus(201);
        $neighborhood = Neighborhood::first();
        $this->assertNotNull($neighborhood);
        $this->assertEquals(Str::title($neighborhood->name), 'Jd Azaleia');
        $this->assertEquals($neighborhood->city_id, $city->id);
    }

    /** @test
     * @group neighborhood
     * @group update
     * @group neighborhood-update
     */
    public function update()
    {
        $country = factory(Country::class)->create();
        $state = factory(State::class)->create(['country_id' => $country->id]);
        $cityAmericana = factory(City::class)->create(['state_id' => $state->id, 'name' => 'Americana']);
        $city = factory(City::class)->create(['state_id' => $state->id]);
        $neighborhood = factory(Neighborhood::class)->create(['city_id' => $city->id, 'name' => 'Curvelo']);
        $this->assertNotNull(State::first());
        $response = $this->patchJson("{$this->baseUrl}/{$neighborhood->id}", ['name' => 'Sumaré', 'city_id' => $cityAmericana->id]);
        $response->assertStatus(200);
        $neighborhood = Neighborhood::first();
        $this->assertNotNull($neighborhood);
        $this->assertEquals(Str::title($neighborhood->name), 'Sumaré');
        $this->assertEquals($neighborhood->city_id, $cityAmericana->id);
    }

    /** @test
     * @group neighborhood
     * @group destroy
     * @group neighborhood-destroy
     */
    public function destroy()
    {
        $country = factory(Country::class)->create();
        $state = factory(State::class)->create(['country_id' => $country->id]);
        $city = factory(City::class)->create(['state_id' => $state->id]);
        $neighborhood = factory(Neighborhood::class)->create(['city_id' => $city->id]);
        $this->assertNotNull(Neighborhood::first());
        $response = $this->deleteJson("{$this->baseUrl}/{$neighborhood->id}");
        $response->assertStatus(200);
        $this->assertNull(Neighborhood::first());
    }
}
