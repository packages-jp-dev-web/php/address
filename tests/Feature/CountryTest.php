<?php

namespace JPAddress\Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use JPAddress\Tests\TestCase;
use Illuminate\Support\Str;
// Models
use JPAddress\Models\Address\Country;
use JPAddress\Models\Address\State;

// When testing inside of a Laravel installation, the base class would be Tests\TestCase
class CountryTest extends TestCase
{
    use RefreshDatabase;
    protected $baseUrl = '/api/country';

    // Use annotation @test so that PHPUnit knows about the test

    /** @test
     * @group country
     * @group validate
     * @group country-validate
     */
    public function validate()
    {
        $dataRequest = ['name' => ''];
        $validation = [
            ['column' => 'name', 'value' => ''],
            ['column' => 'name', 'value' => Str::random(101)],
            ['column' => 'name', 'value' => 'Brasil'],
        ];
        $response = $this->postJson($this->baseUrl, ['name' => 'Brasil']);
        $response->assertStatus(201);

        foreach ($validation as $key => $value) {
            $aux = $dataRequest;
            $aux[$value['column']] = $value['value'];
            $response = $this->postJson($this->baseUrl, $aux);
            $response->assertStatus(422);
        }
        $country = Country::first();
        $response = $this->patchJson("{$this->baseUrl}/{$country->id}", ['name' => 'Brasil']);
        $response->assertStatus(200);
    }

    /** @test
     * @group country
     * @group index
     * @group country-index
     */
    public function index()
    {
        factory(Country::class, 5)->create();
        $country = factory(Country::class)->create(['name' => 'Brasil']);
        $states = factory(State::class, 5)->create(['country_id' => $country->id]);
        $countries = Country::all();
        $response = $this->getJson('/api/country');
        $response->assertStatus(200);
        $dataResult = ['data' => [], 'meta' => [
            'current_page' => 1,
            'from' => 1,
            'last_page' => 1,
            'links' => [
                [
                    'url' => null,
                    'label' => '&laquo; Previous',
                    'active' => false,
                ],
                [
                    'url' => 'http://localhost/api/country?page=1',
                    'label' => '1',
                    'active' => true,
                ],
                [
                    'url' => null,
                    'label' => 'Next &raquo;',
                    'active' => false,
                ],
            ],
            'path' => 'http://localhost/api/country',
            'per_page' => -1,
            'to' => 6,
            'total' => 6,
        ], 'links' => [
            'first' => 'http://localhost/api/country?page=1',
            'last' => 'http://localhost/api/country?page=1',
            'prev' => null,
            'next' => null,
        ], 'included' => []];
        $dataResult['data'] = array_values($countries->map(function ($country) {
            return [
                'type' => 'country',
                'id' => $country->id,
                'attributes' => [
                    'name' => Str::title($country->name),
                    'slug' => $country->slug, 'created_at' => $country->created_at->format('Y-m-d H:i:s'),
                ],
                'relationships' => [],
            ];
        })->toArray());
        $dataApi = json_decode($response->getContent(), true);
        $this->assertEquals($dataResult, $dataApi);
        $this->assertEquals(6, count($dataApi['data']));
        // Test search
        $response = $this->json('GET', $this->baseUrl, ['states' => true, 'search' => 'Brasil']);
        $dataApi = json_decode($response->getContent(), true);
        $this->assertEquals($country->states->map(function ($state) use ($country) {
            return [
                'type' => 'state',
                'id' => $state->id,
                'attributes' => [
                    'name' => Str::title($state->name),
                    'initials' => $state->initials,
                    'slug' => $state->slug, 'created_at' => $state->created_at->format('Y-m-d H:i:s'),
                ],
                'relationships' => [
                    'country' => [
                        'data' => [
                            'type' => 'country',
                            'id' => $country->id,
                        ],
                    ],
                ],
            ];
        })->toArray(), $dataApi['included']);
        $this->assertEquals([
            [
                'type' => 'country',
                'id' => $country->id,
                'attributes' => [
                    'name' => Str::title($country->name),
                    'slug' => $country->slug, 'created_at' => $country->created_at->format('Y-m-d H:i:s'),
                ],
                'relationships' => [
                    'states' => [
                        'data' => $country->states->map(function ($state) {
                            return [
                                'type' => 'state',
                                'id' => $state->id,
                            ];
                        })->toArray(),
                    ],
                ],
            ],
        ], $dataApi['data']);
        $this->assertEquals('brasil', $dataApi['data'][0]['attributes']['slug']);
        // Test paginate
        $response = $this->json('GET', $this->baseUrl, ['paginate' => 2]);
        $dataApi = json_decode($response->getContent(), true);
        $this->assertEquals(2, count($dataApi['data']));
        // Test order
        $dataNews = ['a', 'b', 'c', 'd', 'e'];
        Country::where('name', '!=', '')->delete();
        $this->assertEquals(0, Country::count());
        foreach ($dataNews as $value) {
            Country::create(['name' => $value]);
        }
        $response = $this->json('GET', $this->baseUrl, ['order' => 'asc', 'column' => 'name']);
        $dataApi = json_decode($response->getContent(), true);
        foreach ($dataApi['data'] as $key => $country) {
            $this->assertEquals($dataNews[$key], $country['attributes']['slug']);
        }
        $response = $this->json('GET', $this->baseUrl, ['order' => 'desc', 'column' => 'name']);
        $dataApi = json_decode($response->getContent(), true);
        $i = 4;
        foreach ($dataApi['data'] as $key => $country) {
            $this->assertEquals($dataNews[$i], $country['attributes']['slug']);
            $i--;
        }
    }

    /** @test
     * @group country
     * @group relationships
     * @group country-relationships
     */
    public function relationships()
    {
        $country = factory(Country::class)->create();
        $states = factory(State::class, 2)->create(['country_id' => $country->id]);
        $response = $this->getJson("{$this->baseUrl}/{$country->id}/relationships/states");
        $response->assertStatus(200);
        $dataApi = json_decode($response->getContent(), true);
        $this->assertEquals(2, count($dataApi['data']));
        $this->assertEqualsCanonicalizing($states->map(function ($state) {
            return [
                'type' => 'state', 'id' => $state->id, 'attributes' => ['slug' => $state->slug, 'initials' => $state->initials, 'name' => Str::title($state->name), 'created_at' => $state->created_at->format('Y-m-d H:i:s')],
                'relationships' => ['country' => ['data' => ['type' => 'country', 'id' => $state->country_id]]],
            ];
        })->toArray(), $dataApi['data']);
    }

    /** @test
     * @group country
     * @group show
     * @group country-show
     */
    public function show()
    {
        $country = factory(Country::class)->create();
        $response = $this->getJson("{$this->baseUrl}/{$country->id}");
        $response->assertStatus(200);
        $this->assertEquals([
            'data' => [
                'type' => 'country',
                'id' => $country->id,
                'attributes' => [
                    'name' => Str::title($country->name),
                    'slug' => $country->slug, 'created_at' => $country->created_at->format('Y-m-d H:i:s'),
                ],
                'relationships' => [],
            ],
            'included' => [],
        ], json_decode($response->getContent(), true));
    }

    /** @test
     * @group country
     * @group store
     * @group country-store
     */
    public function store()
    {
        $this->assertNull(Country::first());

        $response = $this->postJson($this->baseUrl, ['name' => 'Brasil']);
        $response->assertStatus(201);
        $country = Country::first();
        $this->assertNotNull($country);
        $this->assertEquals(Str::title($country->name), 'Brasil');
    }

    /** @test
     * @group country
     * @group update
     * @group country-update
     */
    public function update()
    {
        $country = factory(Country::class)->create();
        $this->assertNotNull(Country::first());

        $response = $this->patchJson("{$this->baseUrl}/{$country->id}", ['name' => 'Bogotá']);
        $response->assertStatus(200);
        $country = Country::first();
        $this->assertNotNull($country);
        $this->assertEquals(Str::title($country->name), 'Bogotá');
    }

    /** @test
     * @group country
     * @group destroy
     * @group country-destroy
     */
    public function destroy()
    {
        $country = factory(Country::class)->create();
        $this->assertNotNull(Country::first());

        $response = $this->deleteJson("{$this->baseUrl}/{$country->id}");
        $response->assertStatus(200);
        $country = Country::first();
        $this->assertNull($country);
    }
}
