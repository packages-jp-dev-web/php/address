# Address

<p><a href="https://packagist.org/packages/jeffersonpereira/address" rel="nofollow noindex noopener external ugc"><img src="https://img.shields.io/static/v1?label=packagist&message=v1.0.0&color=blue&style=%3CSTYLE%3E&logo=%3CLOGO%3E" alt="Latest Version on Packagist"></a>
<a href="#" rel="nofollow noindex noopener external ugc"><img src="https://img.shields.io/static/v1?label=license&message=MIT&color=success&style=%3CSTYLE%3E&logo=%3CLOGO%3E" alt="Software License"></a>
</p>
## About package

This package was created with the intention of providing management for the addresses on its website, which uses Laravel from version 6.x. It provides all infrastructure such as: model classes, controllers, migrations and an api with the necessary validations.

## Instalation

<code>composer require "jeffersonpereira/address":"1.\*"</code>

## Documentation

Api documentation will soon be available

## License

<p>
  The Address is open-sourced software licensed under the <a href="http://opensource.org/licenses/MIT" rel="nofollow noindex noopener external ugc">MIT license</a>
</p>
